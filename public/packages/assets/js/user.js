$(function(){
  var $frmR = $("#frm-register");
  $("#frm-register").submit(function(event){
    event.preventDefault();
    if($("#nombre").val() == "" || $("#apellidos").val()=="" || $("email-r").val()==""){
      toastr.info("Favor de llenar todos los campos");
    }else{
      var exp1 = /^[_a-z0-9-]+(.[_a-z0-9-]+)*@dupont.com$/;
      var exp2 = /^[_a-z0-9-]+(.[_a-z0-9-]+)*@pioneer.com$/;
      data = {
        nombre: $("#nombre").val(),
        apellidos: $("#apellidos").val(),
        email : $("#email-r").val()
      }
      if(exp1.test(data.email) || exp2.test(data.email)){
        $.ajax({
          url:"/register",
          method:"POST",
          data: data
        }).done(function(response){
          if(response.status=="200"){
            toastr.success(response.message+', ahora puedes iniciar sesion',"Registro completado.");
            $("#btn-close").trigger("click");
            $frmR[0].reset();
            setTimeout(function(){
                document.location = "/";
            },3500)

            //Document.getElementById('frm-register').reset();
          }else if(response.status=="401-D"){
            console.log(response);
            $.each(response.data,function(object,value){
              toastr.info(value);
            });
          }
        }).fail(function(error){
          console.error(error);
        });
      }else{
        toastr.info("El correo ingresado es incorrecto, tu cuenta de correo tiene que ser 'example@dupont.com o example@pioneer.com'")
      }
    }
  });
  $("#frm-login").submit(function(event){
    event.preventDefault();
    data = {
      email: $("#email").val(),
      password:  $("#password").val()
    };
    $.ajax({
      url:"/login",
      method:"POST",
      data:data
    }).done(function(response){
      if(response.status=="200"){
        toastr.success(response.message,"Nuestros Valores:");
        $("#btn-close").trigger("click");
        document.location = "/";
      }else if(response.status=="D-106"){
        toastr.info("email o contraseña incorrectos");
      }
    }).fail(function(error){
      console.error(error);
    });
  });
});
