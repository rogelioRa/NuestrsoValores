var componentes = {
  tabs:(props)=>{
    let comp =
      '<li class="nav-item">'+
          '<a class="nav-link '+props.show+'" data-toggle="tab" href="#panel-'+props.index+'" role="tab"><span class="hidden-md-up">'+props.index+'</span><span class="hidden-sm-down"><i class="fa fa-chevron-right"></i> '+props.nombre+'</span> </a>'+
      '</li>';
    return comp;
  },
  contentTaps:(props)=>{
    let comp =
      '<div class="tab-pane fade in '+props.show[0]+' '+props.show[1]+'" id="panel-'+props.index+'" role="tabpanel">'+
      '</div>';
    return comp;
  },
  acordion:(props)=>{
    let comp = '<div class="accordion" id="accordion-'+props.index+'" role="tablist" aria-multiselectable="true"></div>';
    return comp;
  },
  itemsAcordion:(props)=>{
    let comp =
      '<div class="card my-card-acordion">'+
          '<div class="card-header" role="tab" id="heading-'+props.index+''+props.indexParent+'">'+
              '<a data-toggle="collapse" data-parent="#accordion-'+props.indexParent+'"   href="#collapse-'+props.index+''+props.indexParent+'" aria-expanded="true" aria-controls="collapse-'+props.index+''+props.indexParent+'">'+
                  '<h5 class="mb-0">'+
                    'Regla '+props.index+' <i class="fa fa-angle-down rotate-icon"></i>'+
                  '</h5>'+
              '</a>'+
          '</div>'+
          '<div id="collapse-'+props.index+''+props.indexParent+'" class="collapse '+props.show+'" role="tabpanel" aria-labelledby="heading-'+props.index+''+props.indexParent+'">'+
              '<div class="card-block">'+
                  props.text+
              '</div>'+
          '</div>'+
      '</div>';
      console.log(comp);
    return comp;
  }
}
