$(document).ready(function(){
	var managerData = {
		getDataFromKey: function(key){
			var datos = null;
			$.each(guia,function(clave,valor){
				if(clave==key){
					datos = valor;
				}
			});
			return datos;
		},
		createDom : function(data){
			$("#my-tabs").empty();
			$("#my-tabs-content").empty();
			$("#title-header-mdl").html("<i class='fa "+data.icon+"'></i> "+data.title);
			var show = ["active","show"];
			var i = 1;
			$.each(data.tabs,function(index,object){
			 	var $tab 		  = $(componentes.tabs({"show":show[0],"index":i,"nombre":object.nombre}));
				var $tabPanel = $(componentes.contentTaps({index:i,show:show}));
				var $acordion = $(componentes.acordion({index:i}));
				for(var j=0;j<object.valores.length;j++){
					var $item = $(componentes.itemsAcordion({indexParent:i,index:j+1,text:object.valores[j],show:show[1]}));
					$acordion.append($item);
					show = ["",""];
				}
				$tabPanel.append($acordion);
				console.log($tabPanel);
				$("#my-tabs").append($tab);
				$("#my-tabs-content").append($tabPanel);
				show = ["","show"];
				i++;
  	   });
		}
	}
	$(".btn-valor").click(function(){
		var key = $(this).data("valor");
		var object = managerData.getDataFromKey(key);
		managerData.createDom(object);
	});
});
