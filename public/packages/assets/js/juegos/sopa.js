var game = new Phaser.Game(920, 490, Phaser.CANVAS, 'app-game', { preload: preload, create: create});

function preload() {
  phaserManager.loadResours();
  game.load.image("container",'/packages/assets/media/images/juegos/sopa/container.png?23');
  game.load.image("tiempo",'/packages/assets/media/images/juegos/sopa/fondoTiempo.png');
  game.load.image('btnPlay','/packages/assets/media/images/juegos/memorama/btn-play1.png');
  game.load.image('btnSalir','/packages/assets/media/images/juegos/memorama/btn-salir.png');
  game.load.image("fondor","/packages/assets/media/images/juegos/fondo-ambiente.png?12");
  game.load.image("fondorespeto","/packages/assets/media/images/juegos/fondo-ambien.png");
  game.load.image('aciertos', '/packages/assets/media/images/juegos/columnas/aciertos.png');
}
var mensajesRe = [
  "¡Reduce la generación de residuos en el hogar y la oficina!",
  "Reúsa los residuos que generas en casa, como envases de plástico, bolsas y otros.",
  "Solicita ayuda cuando no estés seguro de qué acción tomar.",
  "Minimiza tu consumo de energía eléctrica, busca las diferentes formas de lograrlo.",
  "No tires basura en lugares no autorizados o no planeados para ese propósito."
];

var graphics;
var counter=0;
var time;
var duracion = 400;
var textTiempo;
var bestTime = 500;
var isQuestion =false;
var isClick = false;
var aciertos =0;
var txtRed;
var txtFinish;
var btnPlay;
var btnSalir;
var textBestTiempo;
var container;

function create() {
  createdAll();
  game.add.tileSprite(0,0,game.width,game.height,'fondor');
  game.add.sprite(75,3,"NuestrosValoresWhite");
  game.add.sprite(85,129,"aciertos").scale.set(.8);
  fondo            = game.add.image(380,40,"fondorespeto");
  logo             = game.add.image(450,80,'ambiente');
  title            = game.add.text(530, 80,"Respeto por las personas", { font: "1.7rem ",align:"center", fill: "#000", tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 290,fontStyle:"Bold"  } );
  title.visible    = false;
  logo.visible     = false;
  fondo.width      = 500;
  fondo.height     = 380;
  fondo.visible    = false;
  var color        = "#263238";
  var styleM       = { font: "1.1rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 345  };
  txtMessage = game.add.text(475, 160,"Recuerda siempre utilizar el equipo de protección adecuado y mantenerlo en optimas condiciones", styleM);
  game.add.text(39,game.height-50,"Protección del medio ambiente", { font: "1em ",align:"center", fill: "#fff", tabs: [ 150, 150, 200 ],fontStyle:"italic"});
  txtMessage.visible = false;
  btnNext    = game.add.button(599,310,"entendido",pressRes,this);
  btnNext.visible = false;
  btnNext.scale.set(.23);
  btnPlay          = game.add.button(420,300,'btnPlay',actionOnPlay, this, 2, 1, 0);
  btnPlay.visible  = false;
  btnSalir= game.add.button(640,300,'btnSalir',actionSalir,this,2,1,0);
  btnSalir.visible = false;
  createMarco();
  loadLogos();
  loadAudios();
  createdText();
  buttonFullScreen = game.add.button(game.width-70,game.height-68,'fullscreen',actionOnClick, this, 2, 1, 0);
  buttonFullScreen.scale.set(.3);
  container   = game.add.sprite(376,30,"container");
  //game.add.tileSprite(0,0,game.width,game.height,'fondor');
  var style   = { font: "1rem ",align:"center", fill: "#fff", tabs: [ 150, 150, 200 ] };
  game.time.events.loop(Phaser.Timer.SECOND, updateCounter, this);
  sopa.fillSopa();
  sopa.createLetters(sopa.letras);
  sopa.createListLetters();
  agregarPalabras();
  phaserManager.getBestTime("sopa",function(timeBest){
    bestTime = (timeBest === "") ? 500 : parseInt(timeBest);
    textBestTiempo.setText(phaserManager.getMinutsFromSeconds(bestTime));
  });
  graphics = game.add.graphics(0,0);
  graphics.lineStyle(2, 0x76B72B);
  //gameOver("Se ha terminado tu tiempo.\n Haz logrado conseguir:\n "+aciertos+" aciertos, vuelve a intentar");
}
function createdText() {
   //  You can either set the tab size in the style object:
    var colorBox = phaserManager.paleta.white;
    var color    = "#263238";
    var style = { font: "1.8rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ] };
    style.fontStyle ="Bold";
    txtAciertos = game.add.text(165, 150, "00", style);
    style.fontStyle ="";
    style.font ="1rem";
    game.add.text(155,179, "aciertos", style);
    style.fill = "#fff";
    style.fontStyle ="Bold";
    style.font ="2.1rem";
    textTiempo = game.add.text(235,70,"02:50",style);
    style.fontStyle ="";
    style.font ="1.2em";
    game.add.text(238,110,"Tiempo actual",style);
    style.fill = "#fff";
    style.fontStyle ="Bold";
    style.font ="2.1rem";
    textBestTiempo = game.add.text(60,70,"01:50",style);
    style.fontStyle ="";
    style.font ="1.2em";
    game.add.text(65,110,"Mejor tiempo",style);
}
var sopa ={
  palabras:["MEDIOAMBIENTE","CUIDADO","NATURALEZA","COMUNIDAD","SUSTENTABILIDAD","SOCIEDAD","REDUCIR","REUSAR","RECICLAR","RESIDUO","ORGÁNICO","LIMPIEZA","EMISIONES","CONSERVACIÓN","PROTECCIÓN","BIODIVERSIDAD","INNOVACIÓN","AGUA","ECOLÓGICA","RENOVABLES","DESARROLLO"],
  letras: ["a","b","c","d","f","g","h","i","j","k","l","m","n","ñ","o","p","q","r","s","t","u","v","w","x","y","z","ó","á","é","í","ú"],
  txtLetras:[],
  txtPalabras:[],
  sopa:[],
  columnas:0,
  filas:0,
  fillSopa:function(){
    for(var i=0;i<12;i++){
      var row = [];
      for(var j=0;j<18;j++){
        row[j] = sopa.letras[sopa.getRandomInt(0,sopa.letras.length-1)];
      }
      sopa.sopa[i] = row;
    }
  },
  getRandomInt:function(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  },
  addEventHover:function(text){
   text.inputEnabled = true;
   text.events.onInputOver.add(over, this);
   text.events.onInputOut.add(out, this);
   text.events.onInputDown.add(actonOnLetter,this)
  },
  correct(){
    correct.play();
    aciertos++;
    var text = (aciertos<10)? ("0"+aciertos) : aciertos;
    txtAciertos.setText(text);
  },
  wrong: function(){
    wrong.play();
  },
  createLetters: function(letter){
    for(var i=0;i<12;i++){
      var letras = [];
      for(var j=0;j<18;j++){
        var style = { font: "1.2rem ",align:"center", fill: "#000", tabs: [ 150, 150, 200 ] };
        var letra = game.add.text(409+j*25, 60+i*28, sopa.sopa[i][j].toUpperCase() ,style);
        letra.is = "unactive";
        letra.isFound = false;
        letra.cordenadas = [j,i];
        letra.stroke = '#000000';
        letra.strokeThickness = .8;
        letras.push(letra);
        sopa.addEventHover(letra);
      }
      sopa.txtLetras[i] = letras;
    }
    sopa.filas    = sopa.txtLetras.length;
    sopa.columnas = sopa.txtLetras[0].length;

  },
  Verify:function(palabra,xfila,xcolumna,forma){
        var i = 1;
        var j,err;
        err = 0;
        /* si i = 0; ningun error
         * si i = 1; palabra no entra
         * si i = 2; palabra se cruza con otra */
        var tamano = palabra.length;
        switch(forma){
            case 1: //Izquierda
                if(tamano <= xcolumna+1)
                    i = 0;
                if(i == 0){ //Comprueba sino sobrepone otra palabra
                    for(j = 0; j < tamano && err == 0; j++){
                        if(sopa.txtLetras[xfila][xcolumna].is == "active"){
                            if(sopa.txtLetras[xfila][xcolumna].text != palabra[j])
                                err++;
                        }
                        xcolumna--;
                    }
                }
                break;
            case 2: //Derecha
                if(tamano <= sopa.columnas - xcolumna)
                    i = 0;
                if(i == 0){
                    for(j = 0; j < tamano && err == 0; j++){
                        if(sopa.txtLetras[xfila][xcolumna].is == "active"){
                            if(sopa.txtLetras[xfila][xcolumna].text != palabra[j])
                                err++;
                        }
                        xcolumna++;
                    }
                }
                break;
            case 3: //Arriba
                if(tamano <= xfila+1)
                    i = 0;
                if(i == 0){
                    for(j = 0; j < tamano && err == 0; j++){
                        if(sopa.txtLetras[xfila][xcolumna].is == "active"){
                            if(sopa.txtLetras[xfila][xcolumna].text != palabra[j])
                                err++;
                        }
                        xfila--;
                    }
                }
                break;
            case 4: //Abajo
                if(tamano <= sopa.filas - xfila)
                    i = 0;
                if(i == 0){
                    for(j = 0; j < tamano && err == 0; j++){
                        if(sopa.txtLetras[xfila][xcolumna].is == "active"){
                            if(sopa.txtLetras[xfila][xcolumna].text != palabra[j])
                                err++;
                        }
                        xfila++;
                    }
                }
                break;
            case 5: //Arriba Izquierda
                if(tamano <= xcolumna + 1  &&  tamano <= xfila +1)
                    i = 0;
                if(i == 0){
                    for(j = 0; j < tamano && err == 0; j++){
                        if(sopa.txtLetras[xfila][xcolumna].is == "active"){
                            if(sopa.txtLetras[xfila][xcolumna].text != palabra[j])
                                err++;
                        }
                        xfila--;
                        xcolumna--;
                    }
                }
                break;
            case 6: //Arriba Derecha
                if(tamano <= xfila+1 && tamano <= sopa.columnas - xcolumna)
                    i = 0;
                if(i == 0){
                    for(j = 0; j < tamano && err == 0; j++){
                        if(sopa.txtLetras[xfila][xcolumna].is == "active"){
                            if(sopa.txtLetras[xfila][xcolumna].text != palabra[j])
                                err++;
                        }
                        xfila--;
                        xcolumna++;
                    }
                }
                break;
            case 7: //Abajo izquierda
                if(tamano <= sopa.filas - xfila && tamano <= xcolumna+1)
                    i = 0;
                if(i == 0){
                    for(j = 0; j < tamano && err == 0; j++){
                        if(sopa.txtLetras[xfila][xcolumna].is == "active"){
                            if(sopa.txtLetras[xfila][xcolumna].text != palabra[j])
                                err++;
                        }
                        xfila++;
                        xcolumna--;
                    }
                }
                break;
            case 8: //Abajo derecha
                if(tamano <= sopa.filas - xfila && tamano <= sopa.columnas - xcolumna)
                    i = 0;
                if(i == 0){
                    for(j = 0; j < tamano && err == 0; j++){
                        if(sopa.txtLetras[xfila][xcolumna].is == "active"){
                            if(sopa.txtLetras[xfila][xcolumna].text != palabra[j])
                                err++;
                        }
                        xfila++;
                        xcolumna++;
                    }
                }
                break;
        }
        if(err != 0)
            i = 2;
        return i;
  },
  setPalabra: function(palabra,xfila,xcolumna,forma){
        //forma = 1 ; izquierda, 2 derecha, 3 arriba, 4 abajo
        // 5 Arriba izquierda
        // 6  Arriba derecha
        // 7 Abajo izquierda
        // 8 Abajo derecha
        var tamano = palabra.length;
        var j;
        switch(forma){
            case 1:
                for(j = 0; j < tamano; j++){
                    sopa.txtLetras[xfila][xcolumna].setText(palabra[j]);
                    sopa.txtLetras[xfila][xcolumna].is = "active";
                    xcolumna--;
                }
                break;
            case 2:
                for(j = 0; j < tamano; j++){
                    sopa.txtLetras[xfila][xcolumna].setText(palabra[j]);
                    sopa.txtLetras[xfila][xcolumna].is = "active";
                    xcolumna++;
                }
                break;
            case 3:
                for(j = 0; j < tamano; j++){
                        sopa.txtLetras[xfila][xcolumna].setText(palabra[j]);
                        sopa.txtLetras[xfila][xcolumna].is = "active";
                        xfila--;
                    }
                break;
            case 4:
                for(j = 0; j < tamano; j++){
                        sopa.txtLetras[xfila][xcolumna].setText(palabra[j]);
                        sopa.txtLetras[xfila][xcolumna].is = "active";
                        xfila++;
                    }
                break;
            case 5:
                for(j = 0; j < tamano; j++){
                    sopa.txtLetras[xfila][xcolumna].setText(palabra[j]);
                    sopa.txtLetras[xfila][xcolumna].is = "active";
                    xfila--;
                    xcolumna--;
                }
                break;
            case 6:
                for(j = 0; j < tamano; j++){
                    sopa.txtLetras[xfila][xcolumna].setText(palabra[j]);
                    sopa.txtLetras[xfila][xcolumna].is = "active";
                    xfila--;
                    xcolumna++;
                }
                break;
            case 7:
                for(j = 0; j < tamano; j++){
                    sopa.txtLetras[xfila][xcolumna].setText(palabra[j]);
                    sopa.txtLetras[xfila][xcolumna].is = "active";
                    xfila++;
                    xcolumna--;
                }
                break;
            case 8:
                for(j = 0; j < tamano; j++){
                    sopa.txtLetras[xfila][xcolumna].setText(palabra[j]);
                    sopa.txtLetras[xfila][xcolumna].is = "active";
                    xfila++;
                    xcolumna++;
                }
                break;
        }
        //palabras.add(pa);
  },
  createListLetters: function(){
    var resPalabres=[];
    var styleH = { font: "1.5rem ",align:"center", fill: "#fff", tabs: [ 150, 150, 200 ],fontStyle:"Bold" };
    var style = { font: "1.5em ",align:"center", fill: "#fff", tabs: [ 150, 150, 200 ],fontStyle:"Bold"  };
    game.add.text(70,235, "Lista de palabras" ,styleH);
    for(var i=0;i<10;i++){
      var numRand = sopa.getRandomInt(0,sopa.palabras.length);
      var palabra = sopa.palabras[numRand];
      resPalabres.push(palabra);
      var txtPalabra;
      sopa.palabras.splice(numRand,1);
      if(i>4){
        style.align = "left";
        txtPalabra = game.add.text(200,160+i*24,"- "+palabra,style);
      }else{
        txtPalabra = game.add.text(45,280+i*24, "- "+palabra,style);
      }
      txtPalabra.texto = palabra;
      sopa.txtPalabras.push(txtPalabra);
    }
    sopa.palabras = resPalabres;
  }
}


function out(text) {
    //text.tint = 0xcc0000;
    if(!text.isFound){
      game.canvas.style.cursor = "pointer";
      text.fill = "#000";
      text.fontStyle = "";
    }
    //text.setShadow(0, 0, '', 0);
}

function over(text) {
  if(!text.isFound){
    game.canvas.style.cursor = "pointer";
    //text.fill = phaserManager.paleta.red;
    text.fontStyle = "Bold";
  }
    //text.setShadow(5, 5, 'rgba(0,0,0,0.5)', 5);
}
function agregarPalabras(){
  //horizontal
  for(var l=0;l<sopa.palabras.length;l++){
    // get letter aleatoria
    //var longLet     = palabra.length;
  //  console.log(palabra.length);
    isertPalabta(l);
  }
}
function isertPalabta(l){
  try {
    var forma       = sopa.getRandomInt(1,5);
    var palabra     = sopa.palabras[l];
    var x_and_y     = getRandomByForm(forma,palabra);//this return an array.
  //  console.log(y_rand,x_rand,forma);
    var num = sopa.Verify(palabra,x_and_y[1],x_and_y[0],forma);
    if(num==0){
        sopa.setPalabra(palabra,x_and_y[1],x_and_y[0],forma);
        console.log(palabra+" "+forma,x_and_y);
    }else{
      isertPalabta(l);
    }
  } catch (e) {
    //isertPalabta(l);
    exisletter(palabra);
    tacharPalabra(palabra);
    console.error(e);
    console.error("No se crearon las palabras");
  }
}
function getRandomByForm(form,palabra){
  switch (form) {
    case 1://left
      //console.log(palabra.length-1,(sopa.columnas-1));
      x = sopa.getRandomInt(palabra.length-1,sopa.columnas-1); //  7 - 12
      y = sopa.getRandomInt(0,sopa.filas);
      //console.log(x,y);
      return [x,y];
    break;
    case 2:// right
      x = sopa.getRandomInt(0,(sopa.columnas-1)-(palabra.length-1));
      y = sopa.getRandomInt(0,sopa.filas);
      return [x,y];
    break;
    case 3: // up
      if(palabra.length>sopa.filas){
        return getRandomByForm(sopa.getRandomInt(1,3),palabra);
      }else{
        x = sopa.getRandomInt(0,sopa.columnas);
        y = sopa.getRandomInt(palabra.length-1,sopa.filas-1);
//        console.log([x,y],palabra+" Vertical!! up");
        return [x,y];
      }
    break;
    case 4: // down
      if(palabra.length>sopa.filas){
        return getRandomByForm(sopa.getRandomInt(1,3),palabra);
      }else{
        x = sopa.getRandomInt(0,sopa.columnas);
        var margen = (sopa.filas)-(palabra.length);
        y = sopa.getRandomInt(0,(sopa.filas-1)-(palabra.length-1));
  //      console.log([x,y],palabra+" Vertical!! down");
        return [x,y];
      }
    break;
  }
}

function updateCounter(){
  if(!isQuestion){
    if(counter<duracion){
        counter++;
        var time = phaserManager.getMinutsFromSeconds(counter);
        textTiempo.setText(time);
    }else if(counter==duracion){
      gameOver();
    }
  }
}
function actonOnLetter(txt){
  //if(!txt.isFound){
    click.play();
    if(isClick){
      var cordenadaInit = txtRed.cordenadas;
      txtRed.stroke = "#000";
      txtRed.strokeThickness=.3;
      var cordenadaEnd  = txt.cordenadas;
      isCorrect(cordenadaInit,cordenadaEnd);
      isClick = false;
    }else{
      txtRed = txt;
      txt.stroke = "#de77ae";
      txt.strokeThickness = 2;
      isClick = true;
    }
  //}
}

function isCorrect(cordenadaInit,cordenadaEnd){
  var palabra = "";
  console.log(cordenadaInit,cordenadaEnd);
  if(cordenadaInit[0]>cordenadaEnd[0] && cordenadaInit[1]==cordenadaEnd[1]){//in this case the word is left
    console.log(cordenadaInit,cordenadaEnd);
    for (var i = cordenadaInit[0]; i >= cordenadaEnd[0]; i--) {
      palabra += sopa.txtLetras[cordenadaInit[1]][i].text;
    }
  }else if(cordenadaInit[0]<cordenadaEnd[0] && cordenadaInit[1]==cordenadaEnd[1]){//in this case the word is in right
    for (var i = cordenadaInit[0]; i <= cordenadaEnd[0]; i++) {
      palabra += sopa.txtLetras[cordenadaInit[1]][i].text;
    }
  }else if(cordenadaInit[0]==cordenadaEnd[0] && cordenadaInit[1]<cordenadaEnd[1]){ //down
    console.log("down");
    for(var i = cordenadaInit[1]; i <= cordenadaEnd[1]; i++){
      palabra += sopa.txtLetras[i][cordenadaInit[0]].text;
    }
  }else if(cordenadaInit[0]==cordenadaEnd[0] && cordenadaInit[1]>cordenadaEnd[1]){ //up
    console.log("up");
    for(var i = cordenadaInit[1]; i >= cordenadaEnd[1]; i--){
      palabra += sopa.txtLetras[i][cordenadaInit[0]].text;
    }
  }
  console.log(palabra);
  if(exisletter(palabra)){
    //alert("exists");
    var height = (cordenadaInit[0]==cordenadaEnd[0]) ? (28 * palabra.length) : 25;
    var width = (cordenadaInit[1]==cordenadaEnd[1])  ? (25 * palabra.length) : 25;
    var x = (cordenadaInit[0]>cordenadaEnd[0]) ? sopa.txtLetras[cordenadaEnd[1]][cordenadaEnd[0]].position.x-6 : sopa.txtLetras[cordenadaInit[1]][cordenadaInit[0]].position.x-6;
    var y = (cordenadaInit[1]>cordenadaEnd[1]) ? sopa.txtLetras[cordenadaEnd[1]][cordenadaEnd[0]].position.y-1 : sopa.txtLetras[cordenadaInit[1]][cordenadaInit[0]].position.y-1;
    graphics.drawRoundedRect(x,y,width,height,10);
    //rectangles.push(new Phaser.Rectangle(sopa.txtLetras[cordenadaInit[1]][cordenadaInit[0]].position.x-7,sopa.txtLetras[cordenadaInit[1]][cordenadaInit[0]].position.y,width,height));
    pintarPalabra(cordenadaInit,cordenadaEnd);
    tacharPalabra(palabra);
    tacharPalabra(flipWord(palabra));
    sopa.correct();
    if(sopa.palabras.length==0){
      showMessage();
    }
  }else{
    sopa.wrong();
    //alert("no exists");
  }
}
function flipWord(palabra){
  var palabrareverse = "";
  for (var i = palabra.length-1; i >=0; i--) {
    palabrareverse += palabra[i];
  }
  return palabrareverse;
}
function exisletter(palabra){
  var  exists = false;
  var palabrareverse = flipWord(palabra);
  for (var i = 0; i < sopa.palabras.length; i++) {
    if(sopa.palabras[i] == palabra || sopa.palabras[i] == palabrareverse){
      exists = true;
      sopa.palabras.splice(i,1);
      return exists;
    }
  }
  return exists;
}
function tacharPalabra(palabra){
  for (var i = 0; i < sopa.txtPalabras.length; i++) {
    if(sopa.txtPalabras[i].texto == palabra){
      // sopa.txtPalabras
      sopa.txtPalabras[i].fontStyle = "italic";
      sopa.txtPalabras[i].stroke = phaserManager.paleta.red;
      sopa.txtPalabras[i].strokeThickness = .61;
      sopa.txtPalabras[i].fill = phaserManager.paleta.black;
    }
  }
}
function pintarPalabra(ci,ce){
  if(ci[0]>ce[0] && ci[1]==ce[1]){//in this case the word is
    console.log(ci,ce);
    for (var i = ci[0]; i >= ce[0]; i--) {
      pintarLetra(ci[1],i);
    }
  }else if(ci[0]<ce[0] && ci[1]==ce[1]){//in this case the word is in right
    for (var i = ci[0]; i <= ce[0]; i++) {
      pintarLetra(ci[1],i);
    }
  }else if(ci[0]==ce[0] && ci[1]<ce[1]){ //down
    for(var i = ci[1]; i <= ce[1]; i++){
      pintarLetra(i,ci[0]);
    }
  }else if(ci[0]==ce[0] && ci[1]>ce[1]){ //up
    for(var i = ci[1]; i >= ce[1]; i--){
      pintarLetra(i,ci[0]);
    }
  }
}
function pintarLetra(y,x){
  sopa.txtLetras[y][x].stroke = phaserManager.paleta.red;
  sopa.txtLetras[y][x].strokeThickness = .41;
  sopa.txtLetras[y][x].fill = phaserManager.paleta.blue;
  sopa.txtLetras[y][x].fontStyle = "Bold";
  sopa.txtLetras[y][x].isFound = true;
}
function hideAll(){
  for (var i = 0; i < sopa.txtLetras.length; i++) {
    for (var j = 0; j < sopa.txtLetras[0].length; j++) {
      sopa.txtLetras[i][j].destroy();
    }
  }
  container.destroy();
}
function showMessage(){
  showAlert();
  var random        = phaserManager.getRandomInt(0,mensajesRe.length);
  var message      = mensajesRe[random];
  mensajesRe.splice(random,1);
  txtMessage.visible = true;
  txtMessage.setText(message);
  btnNext.visible = true;
}

function showAlert(){
  hideAll();
  graphics.visible = false;
  logo.visible = true;
  fondo.visible = true;
  title.visible = true;
  isQuestion = true;
}
function hideAlert(){
  logo.visible = false;
  fondo.visible = false;
  title.visible = false;
  isQuestion = false;
}
function pressRes(btn){
    click.play();
    hideAlert();
    txtMessage.visible = false;
    btnNext.visible = false;
    setTimeout(function(){
      gameOver();
    },900);
}
function gameOver(){
  showAlert();
  txtMessage.setText("El juego se ha terminado, haz logrado conseguir\n Aciertos: "+aciertos+"\n Tiempo: "+phaserManager.getMinutsFromSeconds(counter));
  txtMessage.visible = true;
  btnPlay.visible = true;
  btnSalir.visible = true;
  phaserManager.saveScore(aciertos,counter,"columnas");
  if(counter<bestTime){
    bestTime = counter;
    textBestTiempo.setText(phaserManager.getMinutsFromSeconds(bestTime));
  }
}
function actionSalir(){
  window.location = "/game";
}
function actionOnPlay(){
  window.location.reload();
}
// function render(){
//   for (var i = 0; i < rectangles.length; i++) {
//     game.debug.geom(rectangles[i],'rgba(118, 183, 43, 0.9)',false);
//   }
// }
