var phaserManager = {
  loadResours: function(){
    game.load.image('fondo', '/packages/assets/media/images/juegos/fondo.jpg?'+(Math.random()*1));
    game.load.image('NuestrosValoresWhite', '/packages/assets/media/images/valores/nuestrosvalores-white-mini.png');
    game.load.image('ambiente', '/packages/assets/media/images/valores/ambiente-min.png');
    game.load.image('ambienteText', '/packages/assets/media/images/valores/ambiente-text.png');
    game.load.image('respeto', '/packages/assets/media/images/valores/respeto-min.png');
    game.load.image('respetoText', '/packages/assets/media/images/valores/respeto-text.png');
    game.load.image('seguridad', '/packages/assets/media/images/valores/seguridad-min.png');
    game.load.image('seguridadText', '/packages/assets/media/images/valores/seguridad-text.png');
    game.load.image('seguridadWhite', '/packages/assets/media/images/valores/seguridad-white.png');
    game.load.image('etica', '/packages/assets/media/images/valores/etica-min.png');
    game.load.image('eticaText', '/packages/assets/media/images/valores/etica-text.png');
    game.load.image('fullscreen','/packages/assets/media/images/juegos/fullscreen/expand.png');
    game.load.audio('click','/packages/assets/media/music/click.mp3');
    game.load.audio('wrong','/packages/assets/media/music/wrong.mp3');
    game.load.audio('correct','/packages/assets/media/music/correct.mp3');
    game.load.image("fondomessage",'/packages/assets/media/images/fondo.png?123');
    game.load.image("entendido",'/packages/assets/media/images/juegos/memorama/btn-next.png?'+(Math.random()*1));
  },
  paleta :{
    blue: "#1D70B7",
    green: "#76B72B",
    purple: "#662482",
    yellow: "#FABA09",
    white: "#fff",
    black: "#263238",
    red : "#E31837"
  },
  shuffle: function (array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // Mientras queden elementos a mezclar...
    while (0 !== currentIndex) {

      // Seleccionar un elemento sin mezclar...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // E intercambiarlo con el elemento actual
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  },
  saveScore: function(aciertos,tiempo,juegoName){
    data = {"aciertos":aciertos,"tiempo":tiempo,"juegoName":juegoName};
  //  console.log(data);
    $.ajax({
      url:"/saveScore",
      type:"post",
      data:data
    }).done(function(response){
      console.log(response);
    });
  },
  getBestTime(nombre,funcion){
    $.ajax({
      url:"/getBestTime/"+nombre,
      type:"get",
    }).done(funcion);
  },
  destroyObjects: function (array){
    for(var i=0;i<array.length;i++){
      array[i].destroy();
    }
    array = [];
  },
  getRandomInt :function(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  },
  getMinutsFromSeconds(time){
    var minutes = Math.floor( time / 60 );
    var seconds = time % 60; 
    //Anteponiendo un 0 a los minutos si son menos de 10
    minutes = minutes < 10 ? '0' + minutes : minutes;
     
    //Anteponiendo un 0 a los segundos si son menos de 10
    seconds = seconds < 10 ? '0' + seconds : seconds;
     
    var result = minutes + ":" + seconds;  // 161:30
    return result;
  }
}
function showMessage(){
  phaserManager.destroyObjects(elements);
  showAlert();
  var random        = phaserManager.getRandomInt(0,messages.length);
  var indexMessage = messages[random];
  messages.splice(random,1);
  var message      = mensajesRe[indexMessage];
  txtMessage.visible = true;
  txtMessage.setText(message);
  btnNext.visible = true;
}

function showAlert(){
  logo.visible = true;
  fondo.visible = true;
  title.visible = true;
  isQuestion = true;
  renderHide();
}
function hideAlert(){
  logo.visible = false;
  fondo.visible = false;
  title.visible = false;
  isQuestion = false;
  lines = [];
}
function pressRes(btn){
    console.log(lines);
    click.play();
    hideAlert();
    txtMessage.visible = false;
    btnNext.visible = false;
    if(nivel<=3){
      createElements();
    }else{
      gameOver();
    }
}
function gameOver(){
  phaserManager.destroyObjects(elements);
  showAlert();
  txtMessage.setText("El juego se ha terminado, haz logrado conseguir\n Aciertos: "+aciertos+"\n Tiempo: "+phaserManager.getMinutsFromSeconds(counter));
  txtMessage.visible = true;
  btnPlay.visible = true;
  btnSalir.visible = true;
  phaserManager.saveScore(aciertos,counter,"columnas");
  if(counter<bestTime){
    bestTime = counter;
    textBestTiempo.setText(phaserManager.getMinutsFromSeconds(bestTime));
  }
}
function actionSalir(){
  window.location = "/game";
}
function actionOnPlay(){
  window.location.reload();
}
