/*WebFontConfig = {

    //  'active' means all requested fonts have finished loading
    //  We set a 1 second delay before calling 'createText'.
    //  For some reason if we don't the browser cannot render the text the first time it's created.
    active: function() { game.time.events.add(Phaser.Timer.SECOND, createText, this); },

    //  The Google Fonts we want to load (specify as many as you like in the array)
    google: {
      families: ['Finger Paint']
    }

};
*/
var seguridad,etica,ambiente,respeto;
var correct;
var wrong;
var click
var buttonFullScreen;



function createdAll(){
  game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
	// add background to game
	//var sprite = game.add.tileSprite(0, 0,game.width,game.height,'fondo');
  game.stage.backgroundColor = "rgb(29,112,183)";
  game.add.tileSprite(0,0,game.width,game.height,'fondo');
  //add icon for value

  //var icon = game.add.sprite(game.width-790,-65,'seguridad');
  //icon.scale.set(1.5);

  //add full screen
  buttonFullScreen = game.add.button(game.width-85,game.height-85,'fullscreen',actionOnClick, this, 2, 1, 0);
  buttonFullScreen.scale.set(.4);
   var logo = game.add.sprite(75,3,"NuestrosValoresWhite");
   //logo.scale.set(.8);
  // console.log(logo.width,logo.height);
  // sets card in view
  createMarco();
  loadLogos();
  loadAudios();
}
function loadLogos(){
  seguridad = game.add.sprite(game.width-320,game.height-50,'seguridad');
  ambiente  = game.add.sprite(game.width-270,game.height-50,'ambiente');
  respeto   = game.add.sprite(game.width-220,game.height-50,'respeto');
  etica     = game.add.sprite(game.width-170,game.height-50,'etica');
}
function loadAudios(){
  click   = game.add.audio("click");
  wrong   = game.add.audio("wrong");
  correct = game.add.audio("correct");
}
function createMarco(){
  //  Create a nice and complex graphics object
   var graphics = game.add.graphics(0, 0);
   graphics.lineStyle(15, 0xE31837);

    graphics.moveTo(12,20);
    graphics.lineTo(60, 20);// up left
    graphics.moveTo(190,20);
    graphics.lineTo(game.width-12,20);// up full
    graphics.moveTo(game.width-20,20);
    graphics.lineTo(game.width-20,game.height-20);  // right full
    graphics.moveTo(20,20);
    graphics.lineTo(20, game.height-15); // left full
    graphics.moveTo(12,game.height-20);
    graphics.lineTo(580, game.height-20); // bottom full
    graphics.moveTo(810,game.height-20);
    graphics.lineTo(game.width-12, game.height-20); // botom left
}




function actionOnClick () {
  if(game.scale.isFullScreen){
    game.scale.stopFullScreen();
  }else{
    game.scale.startFullScreen();
  }
}
