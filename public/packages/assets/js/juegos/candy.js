var game = new Phaser.Game(920, 490, Phaser.CANVAS, 'app-game', { preload: preload, create: create});

var GEM_SIZE = 64;
var GEM_SPACING = 3;
var GEM_SIZE_SPACED = GEM_SIZE + GEM_SPACING;
var BOARD_COLS;
var BOARD_ROWS;
var MATCH_MIN = 3; // min number of same color gems required in a row to be considered a match

var gems;
var selectedGem = null;
var selectedGemStartPos;
var selectedGemTween;
var tempShiftedGem = null;
var allowInput;
var x_gems = 340;
var y_gems = 40;
var counter = 0;
var palabra;
var errors = 0;
var btnsPalabras =[];
var letters = [];
var textTiempo;
var bestTime = 500;
var isQuestion =false;
var aciertos  = 0;
var countNevel = 0;
var nivel  = 1;
var textBestTiempo;
var duracion = 160;
var fondle,fondoOra,txtOracion;
var mconceptos = [];
var fondo,logo,title,txtMessage,txtFinish,btnNext,btnPlay,btnSalir;
var mensajesRe = [
  "Provee información verídica en todo momento.",
  "No compartas información confidencial de la compañía.",
  "Identifica los errores como pasos de aprendizaje para alcanzar el éxito",
  "No trasverses información para favorecer resultados de tu trabajo o de la compañía.",
  "Utiliza todos los bienes que te provee la compañía apropiadamente de acuerdo a los lineamientos. Pregunta o investiga sobre su uso adecuado.",
  "Solicita ayuda cuando no estés seguro de qué acción tomar.",
  "Reporta cualquier caso de violación ética, tanto comportamiento como relación, que notes en tu entorno laboral."
];

function preload() {

  	/*
  	|--------------------------------------------------------------------------
  	| Como crar un nuevo proyecto
  	|--------------------------------------------------------------------------
  	|
  	| En esta parte cargamos todas los recursos: somidos imagenees fondos
  	| para crear los
  	| videojuegos que vayamos ha desarrollar.
  	| El metod phaserManager.loadResours(); nos carga todos los recursos
    | necesarios para crear la estructura estandar del juego
    | esto es un archivo de ejemplo de como se crearía un videojuegos nuevo
    | siguendo la estructura estandar.
    | Para desarrollar un nuevo videojuegos  tendríamos que crear
    | un archivo js con el nombre del juego que vamos a crear por ejemplo
    | memorama.js y dentro de este archivo copiar todo este codigo que se
    | se encuentra en este archivo.
    | para acceder a este juego que acabamos de crear solo tendríasmos que
    | poner en la url: /game/nobre_de_mi_archivo_js.js que acabamos de crear
    | y listo ya tendríamos un nuevo proyecto listo para desarrollar
    | Nota: para acceder a la ruta del juego necesitamos estar previamente
    | logeados en el sistema.
  	*/

    phaserManager.loadResours();
    game.load.spritesheet("GEMS", "/packages/assets/media/images/juegos/candyRain/campanas.png?312490", GEM_SIZE, GEM_SIZE);
    game.load.image("logos", "/packages/assets/media/images/juegos/candyRain/campanas.png");
    game.load.image("fondor","/packages/assets/media/images/juegos/fondo-candy.png");
    game.load.image("fondorespeto","/packages/assets/media/images/juegos/fondo-can.png");
    game.load.image('box', '/packages/assets/media/images/juegos/columnas/box.png?12');
    game.load.image('aciertos', '/packages/assets/media/images/juegos/columnas/aciertos.png');
    game.load.image('btnPlay','/packages/assets/media/images/juegos/memorama/btn-play1.png');
    game.load.image('btnSalir','/packages/assets/media/images/juegos/memorama/btn-salir.png');
}


function create() {

  createdAll();
  game.time.events.loop(Phaser.Timer.SECOND, updateCounter, this);
  game.add.tileSprite(0,0,game.width,game.height,'fondor');
  game.add.sprite(75,3,"NuestrosValoresWhite");
  game.add.sprite(45,159,"aciertos").scale.set(.8);
  fondo            = game.add.image(320,40,"fondorespeto");
  logo             = game.add.image(440,60,'logos');
  title            = game.add.text(450, 130,"Nuestros valores", { font: "1.7rem ",align:"center", fill: "#000", tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 290,fontStyle:"Bold"  } );
  title.visible    = false;
  logo.visible     = false;
  fondo.width      = 500;
  fondo.height     = 380;
  fondo.visible    = false;
  var color        = "#263238";
  var styleM       = { font: "1.1rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 345  };
  txtMessage = game.add.text(406, 180,"Recuerda siempre utilizar el equipo de protección adecuado y mantenerlo en optimas condiciones", styleM);
  game.add.text(39,game.height-50,"Nuestros valores", { font: "1em ",align:"center", fill: "#fff", tabs: [ 150, 150, 200 ],fontStyle:"italic"});
  txtMessage.visible = false;
  btnNext          = game.add.button(545,310,"entendido",pressRes,this);
  btnNext.visible  = false;
  btnNext.scale.set(.23);
  btnPlay          = game.add.button(360,300,'btnPlay',actionOnPlay, this, 2, 1, 0);
  btnPlay.visible  = false;
  btnSalir= game.add.button(590,300,'btnSalir',actionSalir,this,2,1,0);
  btnSalir.visible = false;
  //game.add.text(l.position.x+30,l.position.y+10,"Seguridad y salud", { font: "1rem ",align:"center", fill: "#fff", tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 190  } );
  createMarco();
  //loadLogos();
  loadAudios();
  createdText();
  buttonFullScreen = game.add.button(game.width-85,game.height-75,'fullscreen',actionOnClick, this, 2, 1, 0);
  buttonFullScreen.scale.set(.3);
  mconceptos = conceptos;
  phaserManager.shuffle(mconceptos);
  phaserManager.getBestTime("candy",function(timeBest){
    bestTime = (timeBest === "") ? 500 : parseInt(timeBest);
    textBestTiempo.setText(phaserManager.getMinutsFromSeconds(bestTime));
  });
  // fill the screen with as many gems as possible
  spawnBoard();

  // currently selected gem starting position. used to stop player form moving gems too far.
  selectedGemStartPos = { x: 250, y: 40 };

  // used to disable input while gems are dropping down and respawning
  allowInput = false;

  game.input.addMoveCallback(slideGem, this);

}

function createdText() {
   //  You can either set the tab size in the style object:
    var colorBox = phaserManager.paleta.white;
    var color    = "#263238";
    var style = { font: "1.8rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ] };
    style.fontStyle ="Bold";
    txtAciertos = game.add.text(125, 180, "00", style);
    style.fontStyle ="";
    style.font ="1rem";
    game.add.text(115, 210, "aciertos", style);
    style.fill = "#fff";
    style.fontStyle ="Bold";
    style.font ="2.1rem";
    textTiempo = game.add.text(95,70,"02:50",style);
    style.fontStyle ="";
    style.font ="1.2em";
    game.add.text(115,110,"Tiempo",style);
    style.fill = "#fff";
    style.fontStyle ="Bold";
    style.font ="2.1rem";
    textBestTiempo = game.add.text(95,300,"01:50",style);
    style.fontStyle ="";
    style.font ="1.2em";
    game.add.text(100,340,"Mejor tiempo",style);
}

function updateCounter(){
  if(!isQuestion){
    if(counter<duracion){
        counter++;
        var time = phaserManager.getMinutsFromSeconds(counter);
        textTiempo.setText(time);
    }else if(counter==duracion){
      gameOver();
    }
  }
}
function ok(){
  txtAciertos.setText((aciertos<10)? "0"+aciertos : aciertos);
}
function releaseGem() {

    if (tempShiftedGem === null) {
        selectedGem = null;
        return;
    }

    // when the mouse is released with a gem selected
    // 1) check for matches
    // 2) remove matched gems
    // 3) drop down gems above removed gems
    // 4) refill the board

    var canKill = checkAndKillGemMatches(selectedGem);
    canKill = checkAndKillGemMatches(tempShiftedGem) || canKill;

    if (! canKill) // there are no matches so swap the gems back to the original positions
    {
        var gem = selectedGem;

        if (gem.posX !== selectedGemStartPos.x || gem.posY !== selectedGemStartPos.y)
        {
            if (selectedGemTween !== null)
            {
                game.tweens.remove(selectedGemTween);
            }

            selectedGemTween = tweenGemPos(gem, selectedGemStartPos.x, selectedGemStartPos.y);

            if (tempShiftedGem !== null)
            {
                tweenGemPos(tempShiftedGem, gem.posX, gem.posY);
            }

            swapGemPosition(gem, tempShiftedGem);

            tempShiftedGem = null;

        }
        wrong.play();
    }else{
      correct.play();
    }

    removeKilledGems();

    var dropGemDuration = dropGems();

    // delay board refilling until all existing gems have dropped down
    game.time.events.add(dropGemDuration * 100, refillBoard);

    allowInput = false;

    selectedGem = null;
    tempShiftedGem = null;

}

function slideGem(pointer, x, y) {
    // check if a selected gem should be moved and do it

    if (selectedGem && pointer.isDown)
    {
        var cursorGemPosX = getGemPos(x - x_gems);
        var cursorGemPosY = getGemPos(y - y_gems);

        if (checkIfGemCanBeMovedHere(selectedGemStartPos.x, selectedGemStartPos.y, cursorGemPosX, cursorGemPosY))
        {
            if (cursorGemPosX !== selectedGem.posX || cursorGemPosY !== selectedGem.posY)
            {
                // move currently selected gem
                if (selectedGemTween !== null)
                {
                    game.tweens.remove(selectedGemTween);
                }

                selectedGemTween = tweenGemPos(selectedGem, cursorGemPosX, cursorGemPosY);

                gems.bringToTop(selectedGem);

                // if we moved a gem to make way for the selected gem earlier, move it back into its starting position
                if (tempShiftedGem !== null)
                {
                    tweenGemPos(tempShiftedGem, selectedGem.posX , selectedGem.posY);
                    swapGemPosition(selectedGem, tempShiftedGem);
                }

                // when the player moves the selected gem, we need to swap the position of the selected gem with the gem currently in that position
                tempShiftedGem = getGem(cursorGemPosX, cursorGemPosY);

                if (tempShiftedGem === selectedGem)
                {
                    tempShiftedGem = null;
                }
                else
                {
                    tweenGemPos(tempShiftedGem, selectedGem.posX, selectedGem.posY);
                    swapGemPosition(selectedGem, tempShiftedGem);
                }
            }
        }
    }
}

// fill the screen with as many gems as possible
function spawnBoard() {

    BOARD_COLS = Math.floor((game.world.width-400) / GEM_SIZE_SPACED);
    BOARD_ROWS = Math.floor((game.world.height-50) / GEM_SIZE_SPACED);
    gems = game.add.group();
    gems.x = x_gems;
    gems.y = y_gems;

    for (var i = 0; i < BOARD_COLS; i++){
        for (var j = 0; j < BOARD_ROWS; j++){
            var gem = gems.create((i* GEM_SIZE_SPACED)+350,(j * GEM_SIZE_SPACED)+40, "GEMS");
            gem.position.x = 200;
            gem.name = 'gem' + i.toString() + 'x' + j.toString();
            gem.inputEnabled = true;
            gem.events.onInputDown.add(selectGem, this);
            gem.events.onInputUp.add(releaseGem, this);
            randomizeGemColor(gem);
            setGemPos(gem,i,j); // each gem has a position on the board
            gem.kill();
        }
    }
    removeKilledGems();
    var dropGemDuration = dropGems();
    // delay board refilling until all existing gems have dropped down
    game.time.events.add(dropGemDuration * 100, refillBoard);

    allowInput = false;

    selectedGem = null;
    tempShiftedGem = null;

    // refillBoard();
}

// select a gem and remember its starting position
function selectGem(gem) {

    if (allowInput)
    {
        selectedGem = gem;
        selectedGemStartPos.x = gem.posX;
        selectedGemStartPos.y = gem.posY;
    }

}

// find a gem on the board according to its position on the board
function getGem(posX, posY) {

    return gems.iterate("id", calcGemId(posX, posY), Phaser.Group.RETURN_CHILD);

}

// convert world coordinates to board position
function getGemPos(coordinate) {

    return Math.floor(coordinate / GEM_SIZE_SPACED);

}

// set the position on the board for a gem
function setGemPos(gem, posX, posY) {

    gem.posX = posX;
    gem.posY = posY;
    gem.id = calcGemId(posX, posY);

}

// the gem id is used by getGem() to find specific gems in the group
// each position on the board has a unique id
function calcGemId(posX, posY) {

    return posX + posY * BOARD_COLS;

}

// since the gems are a spritesheet, their color is the same as the current frame number
function getGemColor(gem) {

    return gem.frame;

}

// set the gem spritesheet to a random frame
function randomizeGemColor(gem) {

    gem.frame = game.rnd.integerInRange(0, gem.animations.frameTotal - 1);

}

// gems can only be moved 1 square up/down or left/right
function checkIfGemCanBeMovedHere(fromPosX, fromPosY, toPosX, toPosY) {

    if (toPosX < 0 || toPosX >= BOARD_COLS || toPosY < 0 || toPosY >= BOARD_ROWS)
    {
        return false;
    }

    if (fromPosX === toPosX && fromPosY >= toPosY - 1 && fromPosY <= toPosY + 1)
    {
        return true;
    }

    if (fromPosY === toPosY && fromPosX >= toPosX - 1 && fromPosX <= toPosX + 1)
    {
        return true;
    }

    return false;
}

// count how many gems of the same color lie in a given direction
// eg if moveX=1 and moveY=0, it will count how many gems of the same color lie to the right of the gem
// stops counting as soon as a gem of a different color or the board end is encountered
function countSameColorGems(startGem, moveX, moveY) {

    var curX = startGem.posX + moveX;
    var curY = startGem.posY + moveY;
    var count = 0;

    while (curX >= 0 && curY >= 0 && curX < BOARD_COLS && curY < BOARD_ROWS && getGemColor(getGem(curX, curY)) === getGemColor(startGem))
    {
        count++;
        curX += moveX;
        curY += moveY;
    }

    return count;

}

// swap the position of 2 gems when the player drags the selected gem into a new location
function swapGemPosition(gem1, gem2) {

    var tempPosX = gem1.posX;
    var tempPosY = gem1.posY;
    setGemPos(gem1, gem2.posX, gem2.posY);
    setGemPos(gem2, tempPosX, tempPosY);

}

// count how many gems of the same color are above, below, to the left and right
// if there are more than 3 matched horizontally or vertically, kill those gems
// if no match was made, move the gems back into their starting positions
function checkAndKillGemMatches(gem) {

    if (gem === null) { return; }

    var canKill = false;

    // process the selected gem

    var countUp = countSameColorGems(gem, 0, -1);
    var countDown = countSameColorGems(gem, 0, 1);
    var countLeft = countSameColorGems(gem, -1, 0);
    var countRight = countSameColorGems(gem, 1, 0);

    var countHoriz = countLeft + countRight + 1;
    var countVert = countUp + countDown + 1;

    if (countVert >= MATCH_MIN)
    {
        killGemRange(gem.posX, gem.posY - countUp, gem.posX, gem.posY + countDown);
        canKill = true;
    }

    if (countHoriz >= MATCH_MIN)
    {
        killGemRange(gem.posX - countLeft, gem.posY, gem.posX + countRight, gem.posY);
        canKill = true;
    }

    return canKill;

}

// kill all gems from a starting position to an end position
function killGemRange(fromX, fromY, toX, toY) {

    fromX = Phaser.Math.clamp(fromX, 0, BOARD_COLS - 1);
    fromY = Phaser.Math.clamp(fromY , 0, BOARD_ROWS - 1);
    toX = Phaser.Math.clamp(toX, 0, BOARD_COLS - 1);
    toY = Phaser.Math.clamp(toY, 0, BOARD_ROWS - 1);

    for (var i = fromX; i <= toX; i++)
    {
        for (var j = fromY; j <= toY; j++)
        {
            var gem = getGem(i, j);
            gem.kill();
        }
    }

}

// move gems that have been killed off the board
function removeKilledGems() {

    gems.forEach(function(gem) {
        if (!gem.alive) {
            setGemPos(gem, -1,-1);
        }
    });

}

// animated gem movement
function tweenGemPos(gem, newPosX, newPosY, durationMultiplier) {

    if (durationMultiplier === null || typeof durationMultiplier === 'undefined')
    {
        durationMultiplier = 1;
    }

    return game.add.tween(gem).to({x: newPosX  * GEM_SIZE_SPACED, y: newPosY * GEM_SIZE_SPACED}, 100 * durationMultiplier, Phaser.Easing.Linear.None, true);

}

// look for gems with empty space beneath them and move them down
function dropGems() {

    var dropRowCountMax = 0;

    for (var i = 0; i < BOARD_COLS; i++)
    {
        var dropRowCount = 0;

        for (var j = BOARD_ROWS - 1; j >= 0; j--)
        {
            var gem = getGem(i, j);

            if (gem === null)
            {
                dropRowCount++;
            }
            else if (dropRowCount > 0)
            {
                gem.dirty = true;
                setGemPos(gem, gem.posX, gem.posY + dropRowCount);
                tweenGemPos(gem, gem.posX, gem.posY, dropRowCount);
            }
        }

        dropRowCountMax = Math.max(dropRowCount, dropRowCountMax);
    }

    return dropRowCountMax;

}

// look for any empty spots on the board and spawn new gems in their place that fall down from above
function refillBoard() {

    var maxGemsMissingFromCol = 0;
    var puntos = 0;
    for (var i = 0; i < BOARD_COLS; i++)
    {
        var gemsMissingFromCol = 0;

        for (var j = BOARD_ROWS - 1; j >= 0; j--)
        {
            var gem = getGem(i, j);

            if (gem === null)
            {
                gemsMissingFromCol++;
                gem = gems.getFirstDead();
                gem.reset(i * GEM_SIZE_SPACED, -gemsMissingFromCol * GEM_SIZE_SPACED);
                gem.dirty = true;
                randomizeGemColor(gem);
                setGemPos(gem, i, j);
                tweenGemPos(gem, gem.posX, gem.posY, gemsMissingFromCol * 2);
            }
        }
        maxGemsMissingFromCol = Math.max(maxGemsMissingFromCol, gemsMissingFromCol);
    }
    aciertos = (maxGemsMissingFromCol==0)? aciertos : aciertos+1;
    ok();
    game.time.events.add(maxGemsMissingFromCol * 2 * 100, boardRefilled);

}

// when the board has finished refilling, re-enable player input
function boardRefilled() {
    var canKill = false;
    for (var i = 0; i < BOARD_COLS; i++)
    {
        for (var j = BOARD_ROWS - 1; j >= 0; j--)
        {
            var gem = getGem(i, j);

            if (gem.dirty)
            {
                gem.dirty = false;
                canKill = checkAndKillGemMatches(gem) || canKill;
            }
        }
    }

    if(canKill){
        removeKilledGems();
        var dropGemDuration = dropGems();
        // delay board refilling until all existing gems have dropped down
        game.time.events.add(dropGemDuration * 100, refillBoard);
        allowInput = false;
    } else {
        allowInput = true;
    }
}
function showAlert(){
  logo.visible  = true;
  fondo.visible = true;
  title.visible = true;
  isQuestion    = true;
  gems.visible  = false;
}


function gameOver(){
  showAlert();
  txtMessage.setText("El juego se ha terminado, haz logrado conseguir\n Aciertos: "+aciertos+"\n Tiempo: "+phaserManager.getMinutsFromSeconds(counter));
  txtMessage.visible = true;
  btnPlay.visible = true;
  btnSalir.visible = true;
  phaserManager.saveScore(aciertos,counter,"ahorcado");
  if(counter<bestTime){
    bestTime = counter;
    textBestTiempo.setText(phaserManager.getMinutsFromSeconds(bestTime));
  }
}
function actionSalir(){
  window.location = "/game";
}
function actionOnPlay(){
  window.location.reload();
}
