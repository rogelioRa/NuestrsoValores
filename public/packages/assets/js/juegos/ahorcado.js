wvar game = new Phaser.Game(920, 490, Phaser.CANVAS, 'app-game', { preload: preload, create: create});
var mensajesRe = [
  "Provee información verídica en todo momento.",
  "No compartas información confidencial de la compañía.",
  "Identifica los errores como pasos de aprendizaje para alcanzar el éxito",
  "No trasverses información para favorecer resultados de tu trabajo o de la compañía.",
  "Utiliza todos los bienes que te provee la compañía apropiadamente de acuerdo a los lineamientos. Pregunta o investiga sobre su uso adecuado.",
  "Solicita ayuda cuando no estés seguro de qué acción tomar.",
  "Reporta cualquier caso de violación ética, tanto comportamiento como relación, que notes en tu entorno laboral."
];
var letras = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","ñ","o","p","q","r","s","t","u","v","w","x","y","z"];
var palabras = [
  {concepto:"DESARROLLO",oracion:"Conjunto de normas que dirigen el comportamiento humano dentro de DuPont"},
  {concepto:"ETICA",oracion:"Conjunto de normas que dirigen el comportamiento humano dentro de DuPont"},
  {concepto:"PROFESIONAL",oracion:"Compromiso de desempeñar al máximo tu labor dentro de DuPont"},
  {concepto:"RESPONSABLE",oracion:"Virtud social de caracter individual de formar consciencia de tus actos"},
  {concepto:"COMPROMISO",oracion:"Capacidad de conocer la importancia de cumplir con su trabajo en el periodo acordado"},
  {concepto:"SUPERVISOR",oracion:"Persona con autoridad sobre un grupo de personas que dirige y coordina sus actividades"},
  {concepto:"ESFUERZO",oracion:"Empeño y dedicación que se hace para conseguir algo"},
  {concepto:"LEALTAD",oracion:"Fidelidad a los principios morales de DuPont"},
  {concepto:"PRINCIPIOS",oracion:"Normas que orientan la acción de un ser humano de forma racional"},
  {concepto:"EXCELENCIA",oracion:"Estado de algo con una calidad superior"},
  {concepto:"MISION",oracion:"Motivo, propósito, fin o razón de ser por el que existe una empresa"},
  {concepto:"VISION",oracion:"Exposición clara que indica hacia dónde se dirige la empresa a largo plazo"},
  {concepto:"EMPLEADOS",oracion:"Fuerza de trabajo en DuPont que desempeña una labor a cambio de un sueldo"},
  {concepto:"EXITO",oracion:"Consecuencia de trabajar y hacer las cosas bien"},
  {concepto:"PROOVEDORES",oracion:"Empresas que ofrecen la materia prima a una compañía"},
  {concepto:"BIENES",oracion:"Aquello que puede ser aprovechado por las personas a su beneficio como dinero, coches e inmuebles"},
  {concepto:"LINEAMIENTOS",oracion:"Conjunto de normas que deben seguir todos los integrantes de una compañía"},
  {concepto:"BIENES",oracion:"Aquello que puede ser aprovechado por las personas a su beneficio como dinero, coches e inmuebles"},
  {concepto:"LINEAMIENTOS",oracion:"Conjunto de normas que deben seguir todos los integrantes de una compañía"},
  {concepto:"REGLAS",oracion:"Conjunto de lineamientos que nos señalan como debemos comportarnos"},
  {concepto:"VIOLACION",oracion:"El incumplimiento de algún lineamiento descrito en el Código de Ética es una:"}
];
var counter = 0;
var palabra;
var errors = 0;
var btnsPalabras =[];
var letters = [];
var textTiempo;
var bestTime = 500;
var isQuestion =false;
var boxActive = null;
var aciertos  = 0;
var countNevel = 0;
var nivel  = 1;
var line1;
var buttons = [];
var textBestTiempo;
var duracion = 400;
var fondle,fondoOra,txtOracion;
var mconceptos = [];
var fondo,logo,title,txtMessage,txtFinish,btnNext,btnPlay,btnSalir;
var elements = [];
var base;
var messages = [0,1,2,3,4,5,6];
var personaje =[];
function preload() {
    phaserManager.loadResours();
    game.load.image('base','/packages/assets/media/images/juegos/ahorcado/ahorcado.png');
    game.load.image("fondor","/packages/assets/media/images/juegos/fondo-etica.png");
    game.load.image("fondorespeto","/packages/assets/media/images/juegos/fondo-eti.png");
    game.load.image('box', '/packages/assets/media/images/juegos/columnas/box.png?12');
    game.load.image('aciertos', '/packages/assets/media/images/juegos/columnas/aciertos.png');
    game.load.image('lienzo', '/packages/assets/media/images/juegos/ahorcado/lienzo.png');
    game.load.image('cara', '/packages/assets/media/images/juegos/ahorcado/cara.png?123');
    game.load.image('body', '/packages/assets/media/images/juegos/ahorcado/body.png');
    game.load.image('brazoleft', '/packages/assets/media/images/juegos/ahorcado/brazoleft.png');
    game.load.image('brazoright', '/packages/assets/media/images/juegos/ahorcado/brazoright.png');
    game.load.image('fotterleft', '/packages/assets/media/images/juegos/ahorcado/fotterleft.png');
    game.load.image('fotterright', '/packages/assets/media/images/juegos/ahorcado/fotterright.png');
    game.load.image('btnPlay','/packages/assets/media/images/juegos/memorama/btn-play1.png');
    game.load.image('btnSalir','/packages/assets/media/images/juegos/memorama/btn-salir.png');
    game.load.image('fullscreen','/packages/assets/media/images/juegos/fullscreen/expand-eti.png');
    game.load.image('button','/packages/assets/media/images/juegos/ahorcado/button.png?'+(Math.random()*1));
    game.load.image('fondoLetras','/packages/assets/media/images/juegos/ahorcado/fondoLetras.png?'+(Math.random()*1));
    game.load.image('fondoOra','/packages/assets/media/images/juegos/ahorcado/fondoOra.png?'+(Math.random()*1));
    game.load.image('buttonre','/packages/assets/media/images/juegos/ahorcado/button-re.png?'+(Math.random()*1));
}


function create() {
  createdAll();
  game.add.sprite(200,200,'fondoLetras');
  game.time.events.loop(Phaser.Timer.SECOND, updateCounter, this);
  game.add.tileSprite(0,0,game.width,game.height,'fondor');
  game.add.sprite(75,3,"NuestrosValoresWhite");
  game.add.sprite(45,159,"aciertos").scale.set(.8);
  fondo            = game.add.image(320,40,"fondorespeto");
  logo             = game.add.image(380,80,'etica');
  title            = game.add.text(460, 80,"Altos estándares éticos", { font: "1.7rem ",align:"center", fill: "#000", tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 290,fontStyle:"Bold"  } );
  title.visible    = false;
  logo.visible     = false;
  fondo.width      = 500;
  fondo.height     = 380;
  fondo.visible    = false;
  var color        = "#263238";
  var styleM       = { font: "1.1rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 345  };
  txtMessage = game.add.text(406, 160,"Recuerda siempre utilizar el equipo de protección adecuado y mantenerlo en optimas condiciones", styleM);
  game.add.text(39,game.height-50,"Altos estándares éticos", { font: "1em ",align:"center", fill: "#fff", tabs: [ 150, 150, 200 ],fontStyle:"italic"});
  txtMessage.visible = false;
  btnNext          = game.add.button(545,310,"entendido",pressRes,this);
  btnNext.visible  = false;
  btnNext.scale.set(.23);
  btnPlay          = game.add.button(360,300,'btnPlay',actionOnPlay, this, 2, 1, 0);
  btnPlay.visible  = false;
  btnSalir= game.add.button(590,300,'btnSalir',actionSalir,this,2,1,0);
  btnSalir.visible = false;
  //game.add.text(l.position.x+30,l.position.y+10,"Seguridad y salud", { font: "1rem ",align:"center", fill: "#fff", tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 190  } );
  createMarco();
  loadLogos();
  loadAudios();
  createdText();
  fondle = game.add.sprite(560,90,'fondoLetras');
  fondle.width = 340;
  fondle.height = 280;
  fondOra = game.add.sprite(300,05,'fondoOra');
  txtOracion = game.add.text(345, 40,"Situación en la que el juicio de una persona y la integridad de una acción tienden a estar indebidamente influidos por un interés secundario.", { font: "1.5em ",align:"center", fill: "#000", tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 500,fontStyle:"Bold"  });
  txtOracion.lineSpacing=-3;
  fondOra.scale.set(.83);
  buttonFullScreen = game.add.button(game.width-85,game.height-75,'fullscreen',actionOnClick, this, 2, 1, 0);
  buttonFullScreen.scale.set(.3);
  mconceptos = conceptos;
  phaserManager.shuffle(mconceptos);
  phaserManager.getBestTime("ahorcado",function(timeBest){
    bestTime = (timeBest === "") ? 500 : parseInt(timeBest);
    textBestTiempo.setText(phaserManager.getMinutsFromSeconds(bestTime));
  });
  base = game.add.image(310,110,'base');
  base.scale.set(.49);
  drawPalabras();
  palabra = getPalabra();
  setRespuesta(palabra);
  drawPersonaje();
}

function drawPalabras(){
  var distanciax = 590;
  var salto = 0;
  var x  = distanciax;
  var y  = 120;
  for (var i = 0; i < letras.length; i++) {
    salto++;
    var letra = game.add.button(x,y,'button',presLetter,this);
    letra.check = false;
    x+=40;
    letra.scale.set(.45);
    var txt = game.add.text(letra.position.x+23,letra.position.y+15,letras[i].toUpperCase(),{ font: "1.2rem ", fill: "#fff",fontStyle:"Bold" });
    letters.push(txt);
    letra.txt = txt;
    buttons.push(letra);
    if(salto ==6){
      y += 40;
      salto = 0;
      x = distanciax;
    }
  }
}
function drawPersonaje(){
  var brazoleft = game.add.image(389,207,"brazoleft");
  brazoleft.scale.set(.5);
  var brazoright = game.add.image(418,207,"brazoright");
  brazoright.scale.set(.5);
  var fotterleft = game.add.image(391,245,"fotterleft");
  fotterleft.scale.set(.5);
  var fotterright = game.add.image(411,245,"fotterright");
  fotterright.scale.set(.5);
  var body = game.add.image(404,199,"body");
  body.scale.set(.55);
  var cara = game.add.image(397,149,"cara");
  cara.scale.set(.55);
  personaje = [cara,body,brazoleft,brazoright,fotterleft,fotterright];
  hidePersonaje();
}

function hidePersonaje(){
  console.log(personaje);
  for (var i = 0; i < personaje.length; i++) {
    personaje[i].visible = false;
  }
}
function getPalabra(){
  var aleatoria = phaserManager.getRandomInt(0,palabras.length);
  var palabra =  palabras[aleatoria].concepto;
  txtOracion.setText(palabras[aleatoria].oracion);
  palabras.splice(aleatoria,1);
  return palabra;
}
function setRespuesta(palabra){
  btnsPalabras = [];
  for (var i = 0; i < palabra.length; i++) {
    var btn = game.add.sprite(290+(i*32),360,"buttonre");
    btn.scale.set(.4);
    btnsPalabras.push(btn);
  }
}
function updateCounter(){
  if(!isQuestion){
    if(counter<duracion){
        counter++;
        var time = phaserManager.getMinutsFromSeconds(counter);
        textTiempo.setText(time);
    }else if(counter==duracion){
      gameOver();
    }
  }
}

function createdText() {
   //  You can either set the tab size in the style object:
    var colorBox = phaserManager.paleta.white;
    var color    = "#263238";
    var style = { font: "1.8rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ] };
    style.fontStyle ="Bold";
    txtAciertos = game.add.text(125, 180, "00", style);
    style.fontStyle ="";
    style.font ="1rem";
    game.add.text(115, 210, "aciertos", style);
    style.fill = "#fff";
    style.fontStyle ="Bold";
    style.font ="2.1rem";
    textTiempo = game.add.text(95,70,"02:50",style);
    style.fontStyle ="";
    style.font ="1.2em";
    game.add.text(115,110,"Tiempo",style);
    style.fill = "#fff";
    style.fontStyle ="Bold";
    style.font ="2.1rem";
    textBestTiempo = game.add.text(95,300,"01:50",style);
    style.fontStyle ="";
    style.font ="1.2em";
    game.add.text(100,340,"Mejor tiempo",style);
}
function presLetter(box){
  console.log(box.check);
  if(!box.check){
    box.check = true;
    click.play();
    box.tint = 0xE31837;
    var txt  = box.txt.text;
    var posiciones = checkLetter(txt);
    if(posiciones.length>0){
      setText(posiciones,txt);
      ok();
    }else{
      error();
    }
  }
}
function ok(){
  correct.play();
  aciertos ++;
  cantLetras = (letters.length-27);
  console.log(btnsPalabras.length,cantLetras);
  if(btnsPalabras.length == cantLetras){
    showMessage();
  }
  var text = (aciertos<10)? ("0"+aciertos) : aciertos;
  txtAciertos.setText(text);
}

function error(){
  personaje[errors].visible = true;
  errors++;
  wrong.play();
  if(errors == 6){
    showMessage();
  }
}

function checkLetter(letra){
  console.log(palabra,letra);
  var posiciones = [];
  for (var i = 0; i < palabra.length; i++) {
    if(palabra[i]==letra){
      posiciones.push(i);
    }
  }
  return posiciones;
}

function setText(posiciones,letra){
  console.log(btnsPalabras);
  for (var i = 0; i < posiciones.length; i++) {
    var txtLetra  = game.add.text(btnsPalabras[posiciones[i]].position.x+19,btnsPalabras[posiciones[i]].position.y+10,letra,{ font: "1.4rem ", fill: "#fff",fontStyle:"Bold" });
    btnsPalabras[posiciones[i]].txt = txtLetra;
    letters.push(txtLetra);
  }
}

function destroyButtons(btns){
  for (var i = 0; i < btns.length; i++){
    btns[i].destroy();
  }
}


function showAlert(){
  logo.visible  = true;
  fondOra.visible = false;
  txtOracion.visible = false;
  fondle.visible = false;
  fondo.visible = true;
  title.visible = true;
  isQuestion    = true;
  base.visible  = false;
  destroyButtons(buttons);
  destroyButtons(btnsPalabras);
  hidePersonaje();
}

function hideAlert(){
  logo.visible  = false;
  fondo.visible = false;
  title.visible = false;
  base.visible  = true;
  fondOra.visible = true;
  txtOracion.visible = true;
  fondle.visible = true;
  isQuestion    = false;
}

function showMessage(){
  setTimeout(function(){
    showAlert();
    var random        = phaserManager.getRandomInt(0,messages.length);
    var indexMessage = messages[random];
    messages.splice(random,1);
    var message      = mensajesRe[indexMessage];
    txtMessage.visible = true;
    txtMessage.setText(message);
    btnNext.visible = true;
    phaserManager.destroyObjects(letters);
    letters = [];
  },1000);
}
function pressRes(btn){
    nivel++;
    click.play();
    hideAlert();
    errors = 0;
    txtMessage.visible = false;
    btnNext.visible = false;
    if(nivel<=4){
      drawPalabras();
      palabra = getPalabra();
      setRespuesta(palabra);
    }else{
      gameOver();
    }
}
function gameOver(){
  phaserManager.destroyObjects(elements);
  showAlert();
  txtMessage.setText("El juego se ha terminado, haz logrado conseguir\n Aciertos: "+aciertos+"\n Tiempo: "+phaserManager.getMinutsFromSeconds(counter));
  txtMessage.visible = true;
  btnPlay.visible = true;
  btnSalir.visible = true;
  phaserManager.saveScore(aciertos,counter,"ahorcado");
  if(counter<bestTime){
    bestTime = counter;
    textBestTiempo.setText(phaserManager.getMinutsFromSeconds(bestTime));
  }
}
function actionSalir(){
  window.location = "/game";
}
function actionOnPlay(){
  window.location.reload();
}
