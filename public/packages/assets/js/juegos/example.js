var game = new Phaser.Game(920, 490, Phaser.CANVAS, 'app-game', { preload: preload, create: create});

function preload() {

  	/*
  	|--------------------------------------------------------------------------
  	| Como crar un nuevo proyecto
  	|--------------------------------------------------------------------------
  	|
  	| En esta parte cargamos todas los recursos: somidos imagenees fondos
  	| para crear los
  	| videojuegos que vayamos ha desarrollar.
  	| El metod phaserManager.loadResours(); nos carga todos los recursos
    | necesarios para crear la estructura estandar del juego
    | esto es un archivo de ejemplo de como se crearía un videojuegos nuevo
    | siguendo la estructura estandar.
    | Para desarrollar un nuevo videojuegos  tendríamos que crear
    | un archivo js con el nombre del juego que vamos a crear por ejemplo
    | memorama.js y dentro de este archivo copiar todo este codigo que se
    | se encuentra en este archivo.
    | para acceder a este juego que acabamos de crear solo tendríasmos que
    | poner en la url: /game/nobre_de_mi_archivo_js.js que acabamos de crear
    | y listo ya tendríamos un nuevo proyecto listo para desarrollar
    | Nota: para acceder a la ruta del juego necesitamos estar previamente
    | logeados en el sistema.
  	*/
    phaserManager.loadResours();
}


function create() {
  createdAll();

}
