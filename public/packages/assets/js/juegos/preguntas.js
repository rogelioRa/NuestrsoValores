var preguntas=[
  {
    pregunta:{
      text:"Recuerda siempre utilizar el equipo de protección adecuado y mantenerlo en optimas condiciones",
      respuestas:[
        {
          text:"blba bla bla 1",
          is:true,
        },
        {
          text:"blba bla bla 1",
          is:false,
        },{
          text:"blba bla bla 1",
          is:false,
        }
      ]
    }
  },

  {
    pregunta:{
      text:"¡Hay que estar alerta! identifica el riesgo y respeta los señalamientos de seguridad como piso mojado, acceso restringido, espacio confinado, etc.",
      respuestas:[
        {
          text:"blba bla bla 1",
          is:true,
        },
        {
          text:"blba bla bla 1",
          is:false,
        },{
          text:"blba bla bla 1",
          is:false,
        }
      ]
    }
  },
  {
    pregunta:{
      text:"Antes de utilizar cualquier material químico o agente biológico, informate sobre sus peligros. Sigue las instrucciones de uso del fabricante y los procedimientos de trabajo.",
      respuestas:[
        {
          text:"blba bla bla 1",
          is:true,
        },
        {
          text:"blba bla bla 1",
          is:false,
        },{
          text:"blba bla bla 1",
          is:false,
        }
      ]
    }
  },
  {
    pregunta:{
      text:"Concéntrate en la actividad que vas a realizar antes de comenzarla",
      respuestas:[
        {
          text:"blba bla bla 1",
          is:true,
        },
        {
          text:"blba bla bla 1",
          is:false,
        },{
          text:"blba bla bla 1",
          is:false,
        }
      ]
    }
  },
  {
    pregunta:{
      text:"Al levantar objetos, dobla las rodillas y mantén la parte superior de tu cuerpo recta. Coloca la carga cerca de tu cuerpo, y evita girar cuando levantes el objeto.",
      respuestas:[
        {
          text:"blba bla bla 1",
          is:true,
        },
        {
          text:"blba bla bla 1",
          is:false,
        },{
          text:"blba bla bla 1",
          is:false,
        }
      ]
    }
  },
];
