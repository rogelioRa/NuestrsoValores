conceptos = [
    {
      id:1,
      concepto : "CLIENTE",
      significado: "Persona que utiliza los servicios de una empresa"
    },{
      id:2,
      concepto : "RESPETO",
      significado: "Valor fundamental en DuPont, con el que debemos tratar a todos sin importar su puesto"
    },{
      id:3,
      concepto : "COMPAÑERO",
      significado: "Persona con la que compartes alguna actividad dentro de DuPont"
    },{
      id:4,
      concepto : "CLIMA LABORAL",
      significado: "Calidad del trato entre personas durante el trabajo que genera una mayor productivida"
    },{
      id:5,
      concepto : "INCLUSIÓN",
      significado: "Derecho que tienen los trabajadores de tener una participación, dejando de lado las diferencias"
    },{
      id:6,
      concepto : "TRABAJO EN EQUIPO",
      significado: "Realizar una actividad o trabajo con un grupo de personas organizadas"
    },{
      id:7,
      concepto : "ORGANIZACIÓN",
      significado: "Sistema diseñado para alcanzar las metas, a través de la comunicación interpersonal de forma coordinada"
    },{
      id:8,
      concepto : "RECONOCIMIENTO",
      significado: "Acción de destacar y valorar el trabajo bien hecho de un trabajador que de cierta forma expresa gratitud"
    },{
      id:9,
      concepto : "ENTENDIMIENTO",
      significado: "Facultad de la mente de aprender, entender y razonar para poder tomar decisiones"
    },{
      id:10,
      concepto : "CORDIALIDAD",
      significado: "Característica que tiene una persona que es amable, respetuosa y gentil"
    },{
      id:11,
      concepto : "EMPATÍA",
      significado: "Capacidad de una persona de ponerse en los zapatos de otro"
    },{
      id:12,
      concepto : "COMPETENCIA",
      significado: "Habilidad o destreza para realizar algo en específico"
    },{
      id:13,
      concepto : "COLABORADORES",
      significado: "Personas con las que compartes una obligación y trabajan de manera respetuosa"
    },
    {
      id:14,
      concepto : "CÓDIGO DE CONDUCTA",
      significado: "Principios de la empresa redactados para que el empleado conozca y entienda lo que se espera de él"
    },
    {
      id:15,
      concepto : "COSTUMBRES LOCALES",
      significado: "Tradiciones que tiene un grupo de personas que cuando te incorporas debes adoptar"
    },
    {
      id:16,
      concepto : "CUALIDADES",
      significado: "Características que tiene una persona que lo distingue de los demás"
    },
    {
      id:17,
      concepto : "HABILIDADES",
      significado: "Facilidad para desarrollar un trabajo"
    },
    {
      id:18,
      concepto : "IDEA",
      significado: "Concepto mental que surge a partir de un razonamiento de una persona"
    },{
      id:19,
      concepto : "CREATIVIDAD",
      significado: "Capacidad de la estimular la imaginación con el propósito de generar nuevas ideas"
    },{
      id:20,
      concepto : "SINCERIDAD",
      significado: "Modo de expresarse sin mentiras, honestamente"
    },{
      id:21,
      concepto : "COMPRENSIÓN",
      significado: "Aptitud para entender y justificar algo"
    },{
      id:22,
      concepto : "ENTORNO",
      significado: "Factores que rodean a una persona como condiciones de vida, amigos y trabajo"
    },{
      id:23,
      concepto : "FELICIDAD",
      significado: "Emoción que se produce en la persona cuando alcanza una meta deseada"
    },{
      id:24,
      concepto : "CONDUCTA",
      significado: "Modalidad que tiene una persona para comportarse en diversos ámbitos de su vida"
    },{
      id:25,
      concepto : "AMISTAD",
      significado: "Relación interpersonal y afectiva entre dos o más personas que nace de la confianza"
    },{
      id:26,
      concepto : "DIGNIDIAD",
      significado: "Cualidad de hacerse valer como persona, con responsabilidad, seriedad y respeto"
    },{
      id:27,
      concepto : "EFICAZ",
      significado: "Persona que cumple con el perfil del puesto produciendo el efecto esperado"
    }
];
