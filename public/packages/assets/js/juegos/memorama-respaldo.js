var game = new Phaser.Game(920, 490, Phaser.CANVAS, 'app-game', { preload: preload, create: create});
/*WebFontConfig = {

    //  'active' means all requested fonts have finished loading
    //  We set a 1 second delay before calling 'createText'.
    //  For some reason if we don't the browser cannot render the text the first time it's created.
    active: function() { game.time.events.add(Phaser.Timer.SECOND, createText, this); },

    //  The Google Fonts we want to load (specify as many as you like in the array)
    google: {
      families: ['Finger Paint']
    }

};
*/
var counter  = 0;
var aciertos = 0;
var messagesIndex = [0,1,2,3,4];
var tiempo = 0;
var preguntasRespuestas = [];
var btnSalir;
var textTiempo;
var header
var countCardRotates=0;
var txtAciertos;
var fondo;
var isQuestion = false;
var textBestTiempo;
var buttonFullScreen;
var cardVolteadas = [];
var cards = [];
var pregunta;
var cardsWhite = [];
var cardActive = -1;
var btnPlay;
var txtFinish;
var logo
var bestTime;
var nivel = 0;
var memorama =[
  [
    "1","2","3","4","5","6","7","8","9","10","11","12",
    "1","2","3","4","5","6","7","8","9","10","11","12"
  ],[
    "13","14","15","16","17","18","19","20","1","2","3","4",
    "13","14","15","16","17","18","19","20","1","2","3","4"
  ],[
    "5","6","7","8","9","10","11","12","13","14","15","16",
    "5","6","7","8","9","10","11","12","13","14","15","16"
  ],[
    "17","18","19","20","1","2","3","4","5","6","7","8",
    "17","18","19","20","1","2","3","4","5","6","7","8"
  ]
];

function loadImage(name){
  game.add.image(name,'/packages/assets/media/images/juegos/memorama/'+name+".png?"+(Math.random()*1));
}
function preload() {
    phaserManager.loadResours();
    game.load.image('btnPlay','/packages/assets/media/images/juegos/memorama/btn-play1.png');
    game.load.image('btnSalir','/packages/assets/media/images/juegos/memorama/btn-salir.png');
    game.load.image('cardWhite','/packages/assets/media/images/juegos/memorama/card-white.png');
    game.load.image('logo','/packages/assets/media/images/juegos/dupont-white.png');
    LoadCardMemorama();
}
function LoadCardMemorama(){
  for(var i=0;i<memorama.length;i++){
    for(var j =0;j<memorama[i].length;j++){
        game.load.image(memorama[i][j],'/packages/assets/media/images/juegos/memorama/'+memorama[i][j]+'.png?'+(Math.random()*i));
    }
  }
  for(var i=1;i<5;i++){
    game.load.image('card'+i,'/packages/assets/media/images/juegos/memorama/card'+i+'.png?'+(Math.random()*1));
  }
}

function create() {
  createdAll();
  fondo            = game.add.image(50,50,"fondomessage");
  fondo.width      = 500;
  fondo.height     = 400;
  fondo.visible    = false;
  btnPlay          = game.add.button(100,320,'btnPlay',actionOnPlay, this, 2, 1, 0);
  btnPlay.visible  = false;
  btnSalir= game.add.button(300,320,'btnSalir',actionSalir,this,2,1,0);
  btnSalir.visible = false;
  logo             = game.add.image(120,100,'seguridad');
  logo.visible     = false;
  // sets card in
  ajustCards(nivel+1);
  createdText();
  revolverMemorama();
  phaserManager.getBestTime("memorama",function(timeBest){
    console.log(timeBest);

    bestTime = (timeBest === "") ? 500 : parseInt(timeBest);
    textBestTiempo.setText("Mejor tiempo\n "+phaserManager.getMinutsFromSeconds(bestTime));
  });
}
function actionSalir(btn){
  document.location = "/game";
}
function ajustCards(nivel){
	var x = 50;
	var y = 45;

	for(var i=0;i<24;i++){
    var card = null;
    var logo = null;
		if(i<6){
			 card   = game.add.button(x,y,'card'+1,actionCard, this, 2, 1, 0);
       console.log(card.width,card.height);
    //   logo   = game.add.sprite(x-2,y+15,'seguridadWhite');
		}else if(i>5 && i<12){
       card   = game.add.button(x,y,'card'+1,actionCard, this, 2, 1, 0);
      // logo   = game.add.sprite(x-2,y+15,'seguridadWhite');
		} else if(i>11 && i<18) {
      card   = game.add.button(x,y,'card'+1,actionCard, this, 2, 1, 0);
      //logo   = game.add.sprite(x-2,y+15,'seguridadWhite');
		}else{
      card   = game.add.button(x,y,'card'+1,actionCard, this, 2, 1, 0);
      //logo   = game.add.sprite(x-2,y+15,'seguridadWhite');
    }
    //logo.scale.set(.16);
    card.index = i;
    //card.scale.set(0.3);
    card.width = 80;
    card.height = 100;
    cards.push(card)
		x += 78;
		if(i==5 || i==11 || i == 17){
			x   = 50;
			y  += 102;
		}
	}
}

function createdText() {
    header           = game.add.text(190, 100,"Seguridad y salud", { font: "1.7rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 490,fontStyle:"Bold"  } );
    header.visible   = false;
   //  You can either set the tab size in the style object:
    var colorBox = phaserManager.paleta.white;
    var color    = "#263238";

    var style = { font: "1.8rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ] };
    var styleHash = { font: "1.4rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ] };
    //game.add.text(58,4,"#NuestrosValores",styleHash).setShadow(-1, 2,"rgba(255,255,255,1)" , 0);
    //Timer
    textTiempo = game.add.text(620, 50, "Tiempo actual\n 00:00", style).setShadow(-1, 3, colorBox, 0);
    textTiempo.setShadow(-1, 3, colorBox, 0);
    game.time.events.loop(Phaser.Timer.SECOND, updateCounter, this);

    // Best time
    textBestTiempo =game.add.text(620, 175, "Mejor tiempo\n 00:00", style).setShadow(-1, 3, colorBox, 0);
    textBestTiempo.setShadow(-1, 3, colorBox, 0);

    textAciertos =game.add.text(640, 300, "Aciertos: 0", style).setShadow(-1, 3, colorBox, 0);
    textAciertos.setShadow(-1, 3, colorBox, 0);
}

function updateCounter(){
  if(!isQuestion){
    if(counter<500){
        counter++;
        var time = phaserManager.getMinutsFromSeconds(counter);
        textTiempo.setText("Tiempo actual\n "+time);
    }else if(counter==500){
      gameOver("Tu tiempo se ha acabado, intenta nuevamente");
    }
  }
}

function setAcierto(){
  correct.play();
  aciertos++;
  textAciertos.setText("Aciertos: "+aciertos);
}

function okay(card){
  countCardRotates++;
  setAcierto();
  //destroyObjects(cardsWhite);
  //console.log(cardActive);
  cards[cardActive].destroy();
  card.destroy();
  cardActive=-1;
  cardActive
  cardsWhite=[];
  if(countCardRotates==12){
    //gameOver("Felicidades\n haz logrado completar el juego.\n Volver a jugar");
    setTimeout(nextNevel,2000);
    countCardRotates=0;
  }
}

function actionCard(card) {
  //alert(card.index);
  //card.tint = 0xfffff;
  if(cardsWhite.length<2){
    if(cardActive!=-1){
      if(cards[cardActive].index!=card.index){
        voltearCards(card);
        click.play();
        if(memorama[nivel][(cards[cardActive].index)] == memorama[nivel][card.index]){
          okay(card);
        }else{
          cardActive=-1;
          wrong.play();
          setTimeout(function () {
            destroyObjects(cardsWhite);
          }, 1000);
        }
      }
    }else{
      click.play();
      voltearCards(card);
      cardActive = card.index;


    }
  }
}

function voltearCards(card){
  //var cardWhite = game.add.sprite(card.position.x,card.position.y,'cardWhite');
  //cardWhite.scale.set(0.31);
  var image     = game.add.sprite(card.position.x+6,card.position.y+3,memorama[nivel][card.index]);
  //image.scale.set(0.31);
  image.width   = card.width-8;
  image.height  = card.height-2;
  //cardsWhite.push(cardWhite);
  cardsWhite.push(image);
  cardVolteadas.push(image);
}
function destroyObjects(array){
  for(var i=0;i<array.length;i++){
    array[i].destroy();
  }
  cardsWhite = [];
}
function actionSalir(btn){
  document.location = "/game";
}
function actionOnPlay(btn){
  window.location.reload();
  ajustCards(nivel+1)
  txtFinish.destroy();
  btnSalir.visible = false;
  btn.visible    = false;
  logo.visible   = false;
  header.visible = false;
  fondo.visible  = false;
  counter =0;
  textAciertos.setText("Aciertos: 0");
}
function revolverMemorama(){
  for(var i=0;i<memorama.length;i++){
    phaserManager.shuffle(memorama[i]);
  }
}
function hideCards(){
  for(var i= 0;i<cards.length;i++){
    cards[i].destroy();
  }
  if(cardsWhite.length==1 || cardsWhite.length==2){
      for(var j=0;j<cardsWhite.length;j++){
        cardsWhite[j].destroy();
      }
      cardsWhite = [];
  }
  for(var k=0;k<cardVolteadas.length;k++){
    cardVolteadas[k].destroy();
  }
}
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
function generarPregunta(){
  isQuestion = true;
  var random       = getRandomInt(0,messagesIndex.length);
  indexPregunta    = messagesIndex[random];
  messagesIndex.splice(random,1);
  pregunta         = preguntas[indexPregunta];
  var color        = "#263238";
  var styleP       = { font: "1.4rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 410  };
  var btnEntendido = game.add.button(260,340,"entendido",pressRes,this);
  btnEntendido.scale.set(.23);
  logo.visible     = true;
  header.visible   = true;
  fondo.visible    = true;
  var textPregunta = game.add.text(100, 150,pregunta.pregunta.text, styleP);
  preguntasRespuestas.push(textPregunta);
  preguntasRespuestas.push(btnEntendido);
}
function out(text) {

    text.fill = '#263238';
    text.scale.set(1);
    game.canvas.style.cursor = "default";
}

function over(text) {

    text.fill = phaserManager.paleta.red;
    text.scale.set(1.1);
    game.canvas.style.cursor = "pointer";

}
function pressRes(text){
  click.play();
  nivel++;
  destroyObjects(preguntasRespuestas);
  fondo.visible  = false;
  logo.visible   = false;
  header.visible = false;
  isQuestion = false;
  if(nivel<4){
    ajustCards(nivel+1);
  }else{
    gameOver("Felicidades\n haz logrado completar el juego.")
  }
}
function nextNevel(){
    hideCards();
    generarPregunta();
}

function gameOver(text){
  hideCards();
  messagesIndex = [0,1,2,3,4];
  var color    = "#263238";
  var style = { font: "1.6rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 410  };
  nivel =0;
  fondo.visible = true;
  text = text +"\n Tiempo: "+phaserManager.getMinutsFromSeconds(counter)+"\n Aciertos: "+aciertos;
  logo.visible = true;
  header.visible = true;
  txtFinish    = game.add.text(100, 165,text , style)

  btnPlay.visible = true;
  btnSalir.visible = true;
  //console.log(aciertos,counter);
  if(counter<bestTime){
    bestTime = counter;
    textBestTiempo.setText("Mejor tiempo\n "+phaserManager.getMinutsFromSeconds(bestTime)+" Seg.");
  }
  phaserManager.saveScore(aciertos,counter,"memorama");
  textAciertos.setText("Aciertos: "+aciertos);
  textTiempo.setText("Tiempo actual\n 00:00");
  aciertos =0;
  counter  = 501;
}
