var game = new Phaser.Game(920, 490, Phaser.CANVAS, 'app-game', { preload: preload, create: create,render:render});
var txtAciertos;
var lines =[];
var mensajesRe = [
  "Conoce a tus compañeros, estima sus cualidades y habilidades y procura entablar conexiones.",
  "¡El respeto se demuestra a través de nuestro comportamiento y siempre comienza con uno mismo!",
  "Identifica los errores como pasos de aprendizaje para alcanzar el éxito",
  "Al sentirte seguro y bienvenido en tu lugar de trabajo, apoyas a crear un ambiente en el que otros se sientan igual."
];
var counter = 0;
var textTiempo;
var bestTime = 500;
var isQuestion =false;
var boxActive = null;
var aciertos  = 0;
var countNevel = 0;
var nivel  = 1;
var line1;
var textBestTiempo;
var duracion = 400;
var mconceptos = [];
var fondo,logo,title,txtMessage,txtFinish,btnNext,btnPlay,btnSalir;
var elements = [];
var messages = [0,1,2,3,4];
function preload() {
    phaserManager.loadResours();
    game.load.image("fondor","/packages/assets/media/images/juegos/fondo-re2.png");
    game.load.image("fondorespeto","/packages/assets/media/images/juegos/fondo-respeto.png");
    game.load.image('box', '/packages/assets/media/images/juegos/columnas/box.png?12');
    game.load.image('aciertos', '/packages/assets/media/images/juegos/columnas/aciertos.png');
    game.load.image('btnPlay','/packages/assets/media/images/juegos/memorama/btn-play1.png');
    game.load.image('btnSalir','/packages/assets/media/images/juegos/memorama/btn-salir.png');
}


function create() {
  createdAll();
  circle = new Phaser.Circle(game.world.centerX, 100,64);
  game.time.events.loop(Phaser.Timer.SECOND, updateCounter, this);
  game.add.tileSprite(0,0,game.width,game.height,'fondor');
  game.add.sprite(75,3,"NuestrosValoresWhite");
  game.add.sprite(45,159,"aciertos").scale.set(.8);
  fondo            = game.add.image(320,40,"fondorespeto");
  logo             = game.add.image(380,80,'respeto');
  title            = game.add.text(470, 80,"Respeto por las personas", { font: "1.7rem ",align:"center", fill: "#000", tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 290,fontStyle:"Bold"  } );
  title.visible   = false;
  logo.visible     = false;
  fondo.width      = 500;
  fondo.height     = 380;
  fondo.visible    = false;
  var color        = "#263238";
  var styleM       = { font: "1.1rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 345  };
  txtMessage = game.add.text(399, 160,"Recuerda siempre utilizar el equipo de protección adecuado y mantenerlo en optimas condiciones", styleM);
  game.add.text(39,game.height-50,"Respeto por las personas", { font: "1em ",align:"center", fill: "#fff", tabs: [ 150, 150, 200 ],fontStyle:"italic"});
  txtMessage.visible = false;
  btnNext    = game.add.button(535,310,"entendido",pressRes,this);
  btnNext.visible = false;
  btnNext.scale.set(.23);
  btnPlay          = game.add.button(360,300,'btnPlay',actionOnPlay, this, 2, 1, 0);
  btnPlay.visible  = false;
  btnSalir= game.add.button(590,300,'btnSalir',actionSalir,this,2,1,0);
  btnSalir.visible = false;
  //game.add.text(l.position.x+30,l.position.y+10,"Seguridad y salud", { font: "1rem ",align:"center", fill: "#fff", tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 190  } );
  createMarco();
  loadLogos();
  loadAudios();
  createdText()
  buttonFullScreen = game.add.button(game.width-85,game.height-75,'fullscreen',actionOnClick, this, 2, 1, 0);
  buttonFullScreen.scale.set(.3);
  mconceptos = conceptos;
  phaserManager.shuffle(mconceptos);
  createElements();
  phaserManager.getBestTime("columnas",function(timeBest){
    bestTime = (timeBest === "") ? 500 : parseInt(timeBest);
    textBestTiempo.setText(phaserManager.getMinutsFromSeconds(bestTime));
  });
}
function updateCounter(){
  if(!isQuestion){
    if(counter<duracion){
        counter++;
        var time = phaserManager.getMinutsFromSeconds(counter);
        textTiempo.setText(time);
    }else if(counter==duracion){
      gameOver();
    }
  }
}

function createdText() {
   //  You can either set the tab size in the style object:
    var colorBox = phaserManager.paleta.white;
    var color    = "#263238";
    var style = { font: "1.8rem ",align:"center", fill: color, tabs: [ 150, 150, 200 ] };
    style.fontStyle ="Bold";
    txtAciertos = game.add.text(125, 180, "00", style);
    style.fontStyle ="";
    style.font ="1rem";
    game.add.text(115, 210, "aciertos", style);
    style.fill = "#fff";
    style.fontStyle ="Bold";
    style.font ="2.1rem";
    textTiempo = game.add.text(95,70,"02:50",style);
    style.fontStyle ="";
    style.font ="1.2em";
    game.add.text(115,110,"Tiempo",style);
    style.fill = "#fff";
    style.fontStyle ="Bold";
    style.font ="2.1rem";
    textBestTiempo = game.add.text(95,300,"01:50",style);
    style.fontStyle ="";
    style.font ="1.2em";
    game.add.text(100,340,"Mejor tiempo",style);
}
function createElements(){
  var currentConceptos =[];
  var styleC = { font: "1.2em ",align:"center",fill: "#000" , tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 160,fontStyle:"Bold"  }; // style concept
  var styleS = { font: ".6rem ",align:"center", fill: "#000", tabs: [ 150, 150, 200 ],wordWrap: true, wordWrapWidth: 280 }; // style significado
  //crear conceptos
  console.log(mconceptos,mconceptos.length);
  for (var i = 0; i < 7; i++) {
    var box = game.add.button(290,50+(i*50),"box",boxClick,this);
    var txt = game.add.text(box.position.x+35,box.position.y+15,mconceptos[i].concepto,styleC);
    elements.push(box);
    elements.push(txt);
    box.txt = txt;
    box.enable = true;
    box.id = mconceptos[i].id;
    box.width = 200;
    box.height = 50;
    box.tipo  = "concepto";
    currentConceptos.push(mconceptos[i]);
    console.log(mconceptos[i]);
    mconceptos.splice(i,1);
  }
  // crear Significados
  //phaserManager.shuffle(currentConceptos);
  for (var i = 0; i < currentConceptos.length; i++) {
    var box = game.add.button(540,50+(i*50),"box",boxClick,this);
    var txt = game.add.text(box.position.x+40,box.position.y+10,currentConceptos[i].significado,styleS);
    elements.push(box);
    elements.push(txt);
    box.enable = true;
    box.txt = txt;
    box.id = currentConceptos[i].id;
    box.tipo = "significado";
    box.width = 350;
    box.height = 55;
  }
}

function boxClick(box){
  if(box.enable){
    if(boxActive){
      if(boxActive.tipo !== box.tipo){
        click.play();
        if(boxActive.id == box.id){
          ok();
          line1 = new Phaser.Line(boxActive.x,boxActive.y,box.x,box.y);
          //drawLine(boxActive,box);
          if(boxActive.tipo != "significado"){
            drawLine(boxActive.position.x+boxActive.width-15, boxActive.position.y+(boxActive.height/2), box.position.x+20, box.position.y+(box.height/2));
          }else{
            drawLine(box.position.x+box.width-15, box.position.y+(box.height/2), boxActive.position.x+20, boxActive.position.y+(boxActive.height/2));
          }
          boxActive.tint = 0x76B72B;
          box.tint   = 0x76B72B;
          box.txt.fill = "#fff";
          boxActive.txt.fill = "#fff";
          box.enable = false;
          boxActive.enable = false;
        }else{
          wrong.play();
          boxActive.tint = 16777215;
        }
        boxActive = null;
      }
    }else{
      click.play();
      boxActive = box;
      //alert(box.tint)
      box.tint = 0xFABA09;
    }
  }
}
function ok(){
  aciertos++;
  countNevel++;
  if(countNevel==7){
    showMessage();
    countNevel = 0;
    nivel++;
    phaserManager.destroyObjects(elements);
  }
  var text = (aciertos<9)? ("0"+aciertos) : aciertos;
  correct.play();
  txtAciertos.setText(text);
}
function showMessage(){
  phaserManager.destroyObjects(elements);
  showAlert();
  var random        = phaserManager.getRandomInt(0,messages.length);
  var indexMessage = messages[random];
  messages.splice(random,1);
  var message      = mensajesRe[indexMessage];
  txtMessage.visible = true;
  txtMessage.setText(message);
  btnNext.visible = true;
}

function showAlert(){
  logo.visible = true;
  fondo.visible = true;
  title.visible = true;
  isQuestion = true;
  renderHide();
}
function hideAlert(){
  logo.visible = false;
  fondo.visible = false;
  title.visible = false;
  isQuestion = false;
  lines = [];
}
function pressRes(btn){
    console.log(lines);
    click.play();
    hideAlert();
    txtMessage.visible = false;
    btnNext.visible = false;
    if(nivel<=3){
      createElements();
    }else{
      gameOver();
    }
}
function gameOver(){
  phaserManager.destroyObjects(elements);
  showAlert();
  txtMessage.setText("El juego se ha terminado, haz logrado conseguir\n Aciertos: "+aciertos+"\n Tiempo: "+phaserManager.getMinutsFromSeconds(counter));
  txtMessage.visible = true;
  btnPlay.visible = true;
  btnSalir.visible = true;
  phaserManager.saveScore(aciertos,counter,"columnas");
  if(counter<bestTime){
    bestTime = counter;
    textBestTiempo.setText(phaserManager.getMinutsFromSeconds(bestTime));
  }
}
function actionSalir(){
  window.location = "/game";
}
function actionOnPlay(){
  window.location.reload();
}

// function drawLine(box1,box2){
//   line1.setTo(box1,box2, false);
// }

function drawLine(x1,y1,x2,y2){
   var line = new Phaser.Line(300, 100, 500, 500);
   line.setTo(x1,y1,x2,y2);
   lines.push(line);
   render();
  //line1.fromSprite(handle1, handle2, false);
   //var graphics = game.add.graphics(0,0);
   //graphics.lineStyle(1, 0x662482);
   //graphics.moveTo(x1,y1);
   //graphics.lineTo(x2, y2);// up left
}

function render(){
  if(!isQuestion){
    if(lines.length>0){
      for (var i = 0; i < lines.length; i++) {
        game.debug.geom(lines[i],"#662482");
      }
    }
  }
}
function renderHide(){
  lines = [];
}
