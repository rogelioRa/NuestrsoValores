var guia = {
  "seguridad":{
    icon : "fa-shield",
    title: "Seguridad y Salud | ¡Sé prevenido y procura tu seguridad y salud! ",
    tabs:{
      "dentro":{
        nombre:"Dentro de tu zona de trabajo ",
        "valores":[
          "Familiarízate con la normatividad y estándares de DuPont, así como los procedimientos aplicables a la actividad que realices.",
          "Concéntrate en la actividad que vas a realizar antes de comenzarla: identifica cuáles son algunos puntos de riesgos que puedas encontrarte durante la actividad (espacios, materiales, etc.). ",
          "Antes de utilizar cualquier material químico o agente biológico, informate sobre sus peligros. Sigue las instrucciones de uso del fabricante y los procedimientos de trabajo. ",
          "Utiliza el equipo de protección adecuado a la tarea que vayas a realizar y asegúrate de mantenerlo en óptimas condiciones. ",
          "Al levantar objetos, dobla las rodillas y mantén la parte superior de tu cuerpo recta. Coloca la carga cerca de tu cuerpo, y evita girar cuando levantes el objeto.",
          "Si estás haciendo un trabajo con movimientos repetitivos o una actividad que mantenga tu postura estática, toma descansos frecuentes para que tus músculos se puedan reponer. ",
          "Presta atención, identifica el riesgo y respeta los señalamientos de seguridad como piso mojado, acceso restringido, espacio confinado, etc. "
        ]
      },
      "actividades":{
        nombre:"Actividades cotidianas ",
        "valores":[
          "Cuida tu postura en todo momento, y si tienes alguna molestia, repórtala de inmediato a tu supervisor y al área de salud integral. ",
          "Evita  distracciones al caminar, no vayas leyendo, hablando por celular o enviando mensajes. ",
          "Siempre sujétate del pasamanos al utilizar una escalera. ",
          "Camina solamente por las zonas designadas para peatones y haz contacto visual con conductores de otros vehículos. "
        ]
      },
      "manejo":{
        nombre:"Manejo seguro",
        "valores":[
          "Utiliza el cinturón de seguridad en todo momento, esto aplica para todos los ocupantes del vehículo. ",
          "Al conducir concéntrate en manejar. Evita comer, beber, fumar, programar GPS o música mientras manejas. ",
          "Respeta los límites de velocidad establecidos y mantén una distancia adecuada a las condiciones del camino. No manejes cansado ",
          "Mantén tu vehículo en óptimas condiciones. Asegúrate de que todas las luces funcionen, que la presión de los neumáticos sea adecuada, que los niveles de líquidos sean correctos y contar con toda la documentación adecuada para manejar. ",
          "Reporta todas las condiciones inseguras para que sean corregidas oportunamente. Reporta todos los incidentes para identificar y compartir aprendizajes y prevenir recurrencias. "
        ]
      }
    }
  },
  "ambiente":{
    icon : "fa-pagelines",
    title: "Cuidado del Medio Ambiente  | ¡Contribuye a crear un mundo más seguro, más saludable y más sostenible! ",
    tabs:{
      "residuos":{
        nombre:"Residuos",
        "valores":[
          "Reduce la generación de residuos en el hogar y la oficina. ",
          "Reúsa los residuos que generas en casa, como envases de plástico, bolsas y otros. ",
          "Separa los residuos en orgánicos e inorgánicos y separa los residuos reciclables. Dispón de ellos al servicio de recolección o con proveedores autorizados.",
          "Los que son reciclables llévalos a centros de reciclaje o participa en campañas de reciclaje.",
          "Desechalos de manera segura. Por ejemplo, cuida la manera en la que tiras los objetos con filo (hojas de afeitar, agujas, vidrio roto, etc.).",
          "Minimiza la generación de residuos de comida. ",
        ],
      },
      "hogar":{
          nombre : "En el hogar",
          "valores":[
            "Busca productos de limpieza y otros insumos del hogar que sean amigables conel medio ambiente. ",
            "No viertas materiales peligrosos en el desagüe o en el alcantarillado. Pregunta o investiga cómo puedes disponer correctamente de ese material. ",
            "Reduce las emisiones. Por ejemplo, no utilices productos en aerosol o reduce eluso de insecticidas.",
            "Disminuye el consumo de agua, busca las diferentes formas de lograrlo. ",
            "Minimiza el consumo de energía eléctrica, busca las diferentes formas de lograrlo. "
          ]
        },
        "generales":{
          nombre:"Generales",
          "valores":[
            "Reduce el uso de combustibles en el hogar (gas) y de tu automóvil (gasolina). ",
            "Ayuda a minimizar la contaminación auditiva evitando utilizar el claxon y música a alto volumen. ",
            "No tires basura en lugares no autorizados o no planeados para ese propósito.",
            "Trata de conservar y proteger la biodiversidad en tus destinos turísticos. "
          ]
        }
      }
    },
    "respeto":{
      icon : "fa-user",
      title: "Respeto por las Personas   | ¡El respeto se demuestra a través de nuestro comportamiento y siempre comienza con uno mismo!",
      tabs:{
        "respeto1":{
          nombre:"Respeto 1°",
          valores:[
            "Conoce y entiende lo que se espera de ti en la compañía de acuerdo al Código de Conducta de DuPont. ",
            "Al sentirte seguro y bienvenido en tu lugar de trabajo, apoyas a crear un ambiente en el que otros se sientan igual. ",
            "Aprecia la diversidad en tu entorno, las costumbres locales y valora las áreas de oportunidad que esto representa. ",
            "Conoce a tus compañeros, estima sus cualidades y habilidades y procura entablar conexiones.",
            "Impulsa el desarrollo de ideas y creatividad en tus equipos de trabajo.",
            "Valora los puntos de vista de todos tus compañeros antes de emitir un juicio.",
          ]
        },
        "respeto2":{
          nombre:"Respeto 2°",
          valores:[
            "Escucha con sinceridad las opiniones y participa al final de cada intervención sin interrumpir.",
            "Modera tu volumen y tono de voz cuando expongas tus argumentos con otra persona.",
            "Explica tu desacuerdo en caso de ser necesario, facilita al resto de las personas comprender tu punto de vista.",
            "Reconoce y valora cuando el trabajo está bien realizado.",
            "Identifica los errores como pasos de aprendizaje para alcanzar el éxito.",
            "Dale la oportunidad a tus compañeros de crecer, desarrollarse y contribuir a la  compañía, como en su entorno personal.",
          ]
        }
      }
    },
    "etica":{
      icon : "fa-balance-scale",
      title: "Altos Estándares Éticos",
      tabs:{
        "etica1":{
          nombre:"Ética 1°",
          valores:[
            "Conoce como son las conductas en ética en DuPont-",
            "Si te encuentras en un dilema respecto a una decisión laboral, toma el tiempo de comentar tu preocupación con tu supervisor o algún colaborador. ",
            "Solicita ayuda cuando no estés seguro de qué acción tomar.",
            "En caso de tratar con proveedores internos o externos, asegúrate de que todos los intercambios de dinero o especie estén justificados.",
            "Utiliza todos los bienes que te provee la compañía apropiadamente de acuerdo a los lineamientos. Pregunta o investiga sobre su uso adecuado. ",
            "En caso de ya no hacer uso de alguno de los bienes de la compañía, devuélvelo integramente a los responsables en cuestión.",
            "Provee información verídica en todo momento."
          ]
        },
        "etica2":{
          nombre:"Ética 2°",
          valores:[
            "No compartas información confidencial de la compañía.",
            "No tergiversar información para favorecer resultados de tu trabajo o de la compañía.",
            "Justifica todas las relaciones laborales que establezcas con futuros colaboradores o proveedores para no incurrir en conflicto de intereses.",
            "Asegúrate de actuar siemprede acuerdo a las leyes y regulaciones de tu localidad. Infórmate sobre las legislaciones que conciernen tu trabajo.",
            "No encubras ni toleres actos que deliberadamente violan normas éticas.",
            "Reporta cualquier caso de violación ética, tanto comportamiento como relación, que notes en tu entorno laboral.",
            "Recuerda que, aunque no actúes bajo beneficio propio, las acciones impropias tamién incurren en una conducta no ética."
          ]
        }
      }
    }
};
