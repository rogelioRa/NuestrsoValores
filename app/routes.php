<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home');
});

Route::group(["before"=>"auth"],function(){
	Route::get("/get-reporte-view",'JuegoController@getViewReporte');
	Route::get("/reporte",'JuegoController@getReporte');	
	Route::get("/game/crucigrama",function(){
		return View::make("crucigrama");
	});
	Route::get("/game/{game}",function($game){
		return View::make("game")->with("game",$game);
	});
	Route::post("/saveScore",'JuegoController@save');
	Route::get("/getBestTime/{name}","JuegoController@getBestTime");
	Route::get("/game",function(){
		$juegos = DB::select("select y.id,ifnull(cantidad,0) cantidad,y.nombre,y.alias from
(select uj.id_user userid,count(uj.id) cantidad,j.nombre,j.id from juegos  j
		left join usersJuegos uj on uj.id_juego = j.id
        where uj.id_user = 2
        group by j.nombre,uj.id_user) x
right join
(select id, nombre,alias from juegos) y on y.id = x.id order by id;");
		return View::make("games")->with("juegos",$juegos);
	});
	Route::get("/estadisticas",'JuegoController@estadisticas');
});
Route::post("/register","UserController@register");
Route::post("/login",'LoginController@login');
Route::get("/logout",'LoginController@logout');
