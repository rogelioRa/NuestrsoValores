<?php

class JuegoController extends BaseController {

	public function estadisticas(){
		$estadisticas = DB::select("select user_id,nombre,
	group_concat(distinct if(juego_id=1,puntMin,null)) as 'Memorama',
    group_concat(distinct if(juego_id=2,puntMin,null)) as 'Sopa de letras',
    group_concat(distinct if(juego_id=3,puntMin,null)) as 'Relación de columnas',
    group_concat(distinct if(juego_id=4,puntMin,null)) as 'Ahorcado',
    group_concat(distinct if(juego_id=5,puntMin,null)) as 'Crucigrama',
    group_concat(distinct if(juego_id=6,puntMin,null)) as 'Candy'
 from
(select u.id user_id,concat(u.nombre,' ',u.apellidos) nombre,min(cast(tiempo as unsigned)) puntMin,j.nombre juego,j.id juego_id from users u join usersJuegos uj
		  on u.id = uj.id_user
          join juegos j on j.id = uj.id_juego
          group by j.id,u.id) x
          group by user_id;
       ");
		$reporte = $this->getReporte();
		return View::make("estadisticas",["estadisticas"=>json_encode($estadisticas),"reporte"=>json_encode($reporte)]);
	}
	public function getReporte(){
		return	DB::select("select u.id,concat(u.nombre,' ',u.apellidos) nombre,min(cast(tiempo as unsigned))
		 tiempo,j.nombre juego,uj.aciertos,count(uj.id) veces,
				 date_format(uj.created_at,'%m/%d/%Y %H:%i') fecha
				 from juegos j
	 join usersJuegos uj on uj.id_juego = j.id
		 join users u on u.id = uj.id_user
	group by u.id,j.id order by fecha desc;
	");
	}
 public function getViewReporte(){
	 return View::make("reporte",["reporte"=>json_encode($this->getReporte())]);
 }
	public function save(){
    $id = Auth::user()->id;
		$data = Input::all();
		$rules = [
			"juegoName"=>"required",
			"aciertos"=>"required",
			"tiempo"=>"required"
		];
		$messages = [
			"required" => "El campo :attribute es requerido."
		];
		$validator  =  Validator::make($data,$rules,$messages);
		if($validator->fails()){
			return JuegoController::VALID_ERROR($validator->messages());
		}else{
			$userJuego = new UserJuego($data);
			$juego = Juego::where("nombre","=",$data["juegoName"])->first();
			if($juego){
				$userJuego->id_juego = $juego->id;
				$userJuego->id_user  = $id;
				$userJuego->save();
				return JuegoController::SUCCESS(null);
			}else{
				return JuegoController::WARNING(null);
			}
		}
  }
	public function getBestTime($name){
		$bestTime = DB::select("select min(cast(tiempo as unsigned)) as min from juegos j join usersJuegos uj on uj.id_juego = j.id where j.nombre = '$name';")[0];
		return $bestTime->min;
	}
	public static function VALID_ERROR($data){
		return Response::json(["status"=>"401-D",'statusMessage'=>"validError","message"=>"Fallo al validar algunos datos","data"=>$data]);
	}
	public static function SUCCESS($data){
		return Response::json(["status"=>"200",'statusMessage'=>"success","message"=>"El registro se ha realizadó exitósamente","data"=>$data]);
	}
	public static function WARNING($data){
		return Response::json(["status"=>"201",'statusMessage'=>"warning","message"=>"Algo salio mal durante el registro, consulte con el administrador del portal","data"=>$data]);
	}
}
