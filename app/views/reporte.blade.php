<!DOCTYPE html>
<html>
<head>
    <title>Bootstrap Table Examples</title>
    <link rel="stylesheet" href="/packages/libs/bootstrap-table-master/docs/assets/bootstrap/css/bootstrap.min.css">
    {{HTML::style('packages/libs/font-awesome-4.7.0/css/font-awesome.min.css')}}
    <link rel="stylesheet" href="/packages/libs/bootstrap-table-master/dist/bootstrap-table.min.css">
 <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
 <link rel="stylesheet" href="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/css/bootstrap-editable.css">
 <link rel="stylesheet" href="assets/bootstrap-table/src/bootstrap-table.css">
 {{HTML::script('packages/libs/mdb/js/jquery-3.1.1.min.js')}}
 {{HTML::script('packages/libs/mdb/js/popper.min.js')}}
 {{HTML::script('packages/libs/mdb/js/bootstrap.min.js')}}
 <script src="/packages/libs/bootstrap-table-master/dist/bootstrap-table.min.js" charset="utf-8"></script>
 <script src="/packages/libs/bootstrap-table-master/dist/bootstrap-table-locale-all.min.js" charset="utf-8"></script>
 <script src="/packages/libs/bootstrap-table-master/dist/locale/bootstrap-table-es-MX.min.js" charset="utf-8"></script>
 <script src="/packages/libs/bootstrap-table-master/dist/extensions/export/bootstrap-table-export.min.js" charset="utf-8"></script>
</head>
<body>
  <div class="talbe-report">
    <table  class="table table-striped table-hover msad-table z-depth-1"
           data-toolbar="#toolbar"
           data-search="true"
           data-show-refresh="true"
           data-show-toggle="true"
           data-show-columns="true"
           data-show-export="true"
           data-detail-view="true"
           data-detail-formatter="detailFormatter"
           data-minimum-count-columns="2"
           data-show-pagination-switch="true"
           data-pagination="true"
           data-id-field="id"
           data-page-list="[10, 25, 50, 100, ALL]"
           data-show-footer="false"
           data-side-pagination="server"
           data-url="/examples/bootstrap_table/data"
           data-response-handler="responseHandler"
         data-url="/reporte"
         id="table-report">
      <thead>
          <tr>
              <th data-field="id">ID</th>
              <th data-field="nombre">Nombre de usuario</th>
              <th data-field="juego">Juego</th>
              <th data-field="tiempo">Mejor tiempo</th>
              <th data-field="aciertos">Aciertos</th>
              <th data-field="veces">Veces jugadas</th>
          </tr>
      </thead>
    </table>
  </div>

  <script type="text/javascript">
    $(function(){
      var data = {{$reporte}}
      $('#table-report').bootstrapTable({data:data});
    });
  </script>
</body>
</html>
