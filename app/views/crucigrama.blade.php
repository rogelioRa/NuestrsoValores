@extends('templates.juegos')
@section("title") Juegos capacitación @stop
@section('css')
<style media="screen">
  #marco{
    padding-left: 0;
    padding-top: 0;
  }
</style>
@stop
@section('content-main')
<iframe src="/iframes/DupontCrucigraa1js.html" width="100%" height="101%" style="border:none;margin-top:-1rem;"></iframe>
@stop
@section('js')
<script src="/packages/assets/js/juegos/game.js" charset="utf-8"></script>
<script src="/packages/assets/js/juegos/preguntas.js" charset="utf-8"></script>
<script src="/packages/assets/js/juegos/conceptos.js" charset="utf-8"></script>
@stop
