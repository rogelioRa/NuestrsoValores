@extends('templates.master')
@section("title") Campaña dupont @stop
@section('carousel-section')
<div id="carousel-example-1" class="carousel slide carousel-fade " data-ride="carousel" data-ride="carousel"  style="margin-top:0rem;">
        <!--Indicators-->

        <!--/.Indicators-->

        <!--Slides-->
        <div class="carousel-inner" role="listbox">

            <!--First slide-->
            <div class="carousel-item active">
                <!--Mask-->
                @if(!Auth::user())
                <img src="/packages/assets/media/images/btn-juega.png" alt="" data-toggle="modal" data-target="#mdl-login-register" class="btn-juega animated bounce">
                @else
                <a href="/game/">
                  <img src="/packages/assets/media/images/btn-juega.png" alt="" class="btn-juega animated bounce">
                </a>
                @endif
                <!--/.Mask-->
            </div>
            <!--/.First slide-->
            @if(Auth::user())
            <!--Second slide -->
            <!-- <div class="carousel-item"> -->
                <!--Mask-->
                <!--/.Mask-->
            <!-- </div> -->
            <!--/.Second slide -->


            @endif
        </div>

        <!--/.Controls-->
    </div>
<!-- / Modal de login -->
@stop
@section('content-main')
  <!--Modal: Login / Register Form-->

  <!--Modal: Login / Register Form-->
<div class="container">
  <section id="our-valors" class="section feature-box mt-4 mb-4">
        <!--Section heading-->
        <h1 class="section-heading my-5 pt-4 wow fadeIn" data-wow-delay="0.2s">#NuestrosValores</h1>
        <!--Section sescription-->
        <!--First row-->
        <div class="row features-big">
            <!--First column-->
            <div class="col-md-3 mb-5 wow fadeInUp" data-wow-delay="0.9s">
                <img src="/packages/assets/media/images/valores/seguridad.png?234" class="img-fluid img-valor" alt="">
                <h4 class="feature-title grey-text-2">Seguridad y Salud</h4>
                <p class="grey-text-2">Compartimos el compromiso personal y profesional
de proteger la seguridad y salud de nuestros empleados dentro y fuera
del trabajo, de nuestros clientes, contratistas, y la de las comunidades
en las que operamos.</p>
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-md-3 mb-1 wow fadeInDown" data-wow-delay="0.9s">
                <img src="/packages/assets/media/images/valores/ambiente.png?34" class="img-fluid img-valor" alt="">
                <h4 class="feature-title grey-text-2">Protección del Medio Ambiente</h4>
                <p class="grey-text-2">Desarrollamos soluciones sustentables
para nuestros clientes, siempre manejando los negocios de manera
responsable para proteger y preservar el medio ambiente y los recursos
naturales tanto para las generaciones de hoy como las futuras.</p>
            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-md-3 mb-1 wow fadeInUp" data-wow-delay="0.9s">
                <img src="/packages/assets/media/images/valores/respeto.png" class="img-fluid img-valor" alt="">
                <h4 class="feature-title grey-text-2">Respeto por las personas</h4>
                <p class="grey-text-2 ">Tratamos a los empleados y colegas con
profesionalismo, dignidad y respeto, creando un ambiente donde las
personas puedan contribuir, innovar y sobresalir</p>
            </div>
            <!--/Third column-->
            <!--Four column-->
            <div class="col-md-3 mb-1 wow fadeInDown" data-wow-delay="0.9s">
                <img src="/packages/assets/media/images/valores/etica.png" class="img-fluid img-valor" alt="">
                <h4 class="feature-title grey-text-2">Altos Estándares Éticos</h4>
                <p class="grey-text-2">Conducimos nuestras relaciones y negocios
de acuerdo con los más altos estándares éticos, bajo las leyes
aplicables, buscando ser socialmente reconocidos como ciudadanos
corporativos.</p>
            </div>
            <!--/Four column-->
        </div>
        <!--/First row-->

    </section>
</div>
<hr>
<div class="container">
  <section id="cont-seg" class="section feature-box mt-4 mb-4">
        <!--Section heading-->
        <!-- <h1 class="section-heading my-5 pt-4 wow fadeIn" data-wow-delay="0.2s">Tips de seguridad <i class="fa fa-user-circle"></i></h1> -->
        <div class="row">
          <div class="col-md-5">
            <div class="">
                <div class="text-center white-text wow fadeInLeft" data-wow-delay="0.6s">
                    <img src="/packages/assets/media/images/valores/seguridad-text.png?234" class="img-fluid" alt="">
                </div>
            </div>
          </div>
          <div class="col-md-7">
            <ul class="nav md-pills nav-justified my-pills pills-secondary pills-seguridad" style="margin-left:3rem;margin-bottom:.1rem;">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#panel-se-1" role="tab">Dentro de tu zona de trabajo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel-se-2" role="tab">Actividades cotidianas </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel-se-3" role="tab">Manejo seguro</a>
                </li>
            </ul>
            <div class="tab-content">
              <!--Panel 1-->
              <div class="tab-pane fade in show active" id="panel-se-1" role="tabpanel">
                <ul class="list- text-justify wow fadeInRight" data-wow-delay="0.6s">
                  <li class="list-item">Familiarízate con la normatividad y estándares de DuPont, así como los procedimientos aplicables a la actividad que realices.</li>
                  <li class="list-item">Concéntrate en la actividad que vas a realizar antes de comenzarla: identifica cuáles son algunos puntos de riesgos que puedas encontrarte durante la actividad (espacios, materiales, etc.).</li>
                  <li class="list-item">Antes de utilizar cualquier material químico o agente biológico, infórmate sobre sus peligros. Sigue las instrucciones de uso del fabricante y los procedimientos de trabajo.</li>
                  <li class="list-item">Utiliza el equipo de protección adecuado a la tarea que vayas a realizar y asegúrate de mantenerlo en óptimas condiciones. </li>
                  <li class="list-item">Al levantar objetos, dobla las rodillas y mantén la parte superior de tu cuerpo recta. Coloca la carga cerca de tu cuerpo, y evita girar cuando levantes el objeto.</li>
                </ul>
              </div>
              <!--panel1-->
              <!--Panel 2-->
              <div class="tab-pane fade in" id="panel-se-2" role="tabpanel">
                <ul class="list- text-justify wow fadeInRight" data-wow-delay="0.6s">
                  <li class="list-item">Cuida tu postura en todo momento, y si tienes alguna molestia, repórtala de inmediato a tu supervisor y al área de salud integral. </li>
                  <li class="list-item">Evita  distracciones al caminar, no vayas leyendo, hablando por celular o enviando mensajes.</li>
                  <li class="list-item">Siempre sujétate del pasamanos al utilizar una escalera.</li>
                  <li class="list-item">Camina solamente por las zonas designadas para peatones y haz contacto visual con conductores de otros vehículos. </li>
                </ul>
              </div>
              <!--panel 2-->
              <!--Panel 3-->
              <div class="tab-pane fade in" id="panel-se-3" role="tabpanel">
                <ul class="list- text-justify wow fadeInRight" data-wow-delay="0.6s">
                  <li class="list-item">Utiliza el cinturón de seguridad en todo momento, esto aplica para todos los ocupantes del vehículo.</li>
                  <li class="list-item">Al conducir concéntrate en manejar. Evita comer, beber, fumar, programar GPS o música mientras manejas.</li>
                  <li class="list-item">Respeta los límites de velocidad establecidos y mantén una distancia adecuada a las condiciones del camino. No manejes cansado.</li>
                  <li class="list-item">Mantén tu vehículo en óptimas condiciones. Asegúrate de que todas las luces funcionen, que la presión de los neumáticos sea adecuada, que los niveles de líquidos sean correctos y contar con toda la documentación adecuada para manejar  </li>
                  <li class="list-item">Reporta todas las condiciones inseguras para que sean corregidas oportunamente. Reporta todos los incidentes para identificar y compartir aprendizajes y prevenir recurrencias.</li>
                </ul>
              </div>
              <!--panel 3-->
            </div>
          </div>
        </div>
  </section>
</div>
<div class="streak streak-photo streak-md hm-indigo-light" style="background-image: url('/packages/assets/media/images/slide2.png?9012'); height:300px;">
    <div class="flex-center mask pattern-1">
        <div class="text-center white-text">
            <h2 class="h2-responsive mb-5"><strong>   </strong></h2>
        </div>
    </div>
</div>
<div class="container">
  <section id="cont-amb" class="section feature-box mt-4 mb-4">
        <!--Section heading-->
        <!-- <h1 class="section-heading my-5 pt-4 wow fadeIn" data-wow-delay="0.2s">Tips para cuidar el ambiente <i class="fa fa-envira"></i></h1> -->
        <div class="row">
          <div class="col-md-5">
            <div class="">
                <div class="text-center white-text wow fadeInLeft" data-wow-delay="0.6s">
                    <img src="/packages/assets/media/images/valores/ambiente-text.png?234" class="img-fluid" alt="">
                </div>
            </div>
          </div>
          <div class="col-md-7">
            <ul class="nav md-pills nav-justified my-pills pills-secondary pills-ambiente">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#panel-am-1" role="tab">Residuos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel-am-2" role="tab">En el hogar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel-am-3" role="tab">Generales</a>
                </li>
            </ul>
            <div class="tab-content">
              <!-- panel 1-->
              <div class="tab-pane fade in show active" id="panel-am-1" role="tabpanel">
                <ul class="list- text-justify wow fadeInRight" data-wow-delay="0.6s">
                  <li class="list-item">Reduce la generación de residuos en el hogar y la oficina. </li>
                  <li class="list-item">Reúsa los residuos que generas en casa, como envases de plástico, bolsas y otros.</li>
                  <li class="list-item">Separa los residuos en orgánicos e inorgánicos y separa los residuos reciclables. Dispón de ellos al servicio de recolección o con proveedores autorizados.</li>
                  <li class="list-item">Deséchalos de manera segura. Por ejemplo, cuida la manera en la que tiras los objetos con filo (hojas de afeitar, agujas, vidrio roto, etc.).</li>
                  <li class="list-item">Minimiza la generación de residuos de comida.</li>
                </ul>
              </div>
              <!--panel 1-->
              <!-- panel 2 -->
              <div class="tab-pane fade in" id="panel-am-2" role="tabpanel">
                <ul class="list- text-justify wow fadeInRight" data-wow-delay="0.6s">
                  <li class="list--item">Busca productos de limpieza y otros insumos del hogar que sean amigables con el medio ambiente. </li>
                  <li class="list--item">No viertas materiales peligrosos en el desagüe o en el alcantarillado. Pregunta o investiga cómo puedes disponer correctamente de ese material.</li>
                  <li class="list--item">Reduce las emisiones. Por ejemplo, no utilices productos en aerosol o reduce el uso de insecticidas.</li>
                  <li class="list--item">Disminuye el consumo de agua, busca las diferentes formas de lograrlo. </li>
                  <li class="list--item">Minimiza el consumo de energía eléctrica, busca las diferentes formas de lograrlo. </li>
                </ul>
              </div>
              <!-- panel 2 -->
              <!-- panel 3 -->
              <div class="tab-pane fade in" id="panel-am-3" role="tabpanel">
                <ul class="list- text-justify wow fadeInRight" data-wow-delay="0.6s">
                  <li class="list--item">Reduce el uso de combustibles en el hogar (gas) y de tu automóvil (gasolina).</li>
                  <li class="list--item">Ayuda a minimizar la contaminación auditiva evitando utilizar el claxon y música a alto volumen. </li>
                  <li class="list--item">No tires basura en lugares no autorizados o no planeados para ese propósito.</li>
                  <li class="list--item">Trata de conservar y proteger la biodiversidad en tus destinos turísticos.  </li>
                </ul>
              </div>
              <!-- panel 3 -->
            </div>
          </div>
        </div>
  </section>
</div>
<div class="streak streak-photo streak-md hm-indigo-light" style="background-image: url('/packages/assets/media/images/slide3.png?9012'); height:300px;">
    <div class="flex-center mask pattern-1">
        <div class="text-center white-text">
            <h2 class="h2-responsive mb-5"><strong>   </strong></h2>
        </div>
    </div>
</div>
<div class="container">
  <section id="cont-resp" class="section feature-box mt-4 mb-4">
        <!--Section heading-->
        <!-- <h1 class="section-heading my-5 pt-4 wow fadeIn" data-wow-delay="0.2s">Tips de respeto <i class="fa fa-handshake-o"></i></h1> -->
        <div class="row">
          <div class="col-md-5">
            <div class="">
                <div class="text-center white-text wow fadeInLeft" data-wow-delay="0.6s">
                    <img src="/packages/assets/media/images/valores/respeto-text.png?234" class="img-fluid" alt="">
                </div>
            </div>
          </div>
          <div class="col-md-7">
            <ul class="nav md-pills nav-justified my-pills pills-secondary" style="padding-left:3rem;padding-right:3rem;">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#panel-re-1" role="tab">Parte uno</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel-re-2" role="tab">Parte dos</a>
                </li>
            </ul>
            <div class="tab-content">
              <!-- panel 1-->
              <div class="tab-pane fade in show active" id="panel-re-1" role="tabpanel">
                <ul class="list- text-justify wow fadeInRight" data-wow-delay="0.6s">
                  <li class="list-item">Conoce y entiende lo que se espera de ti en la compañía de acuerdo al Código de Conducta de DuPont. </li>
                  <li class="list-item">Al sentirte seguro y bienvenido en tu lugar de trabajo, apoyas a crear un ambiente en el que otros se sientan igual. </li>
                  <li class="list-item">Aprecia la diversidad en tu entorno, las costumbres locales y valora las áreas de oportunidad que esto representa. </li>
                  <li class="list-item">Conoce a tus compañeros, estima sus cualidades y habilidades y procura entablar conexiones.</li>
                  <li class="list-item">Impulsa el desarrollo de ideas y creatividad en tus equipos de trabajo.</li>
                  <li class="list-item">Valora los puntos de vista de todos tus compañeros antes de emitir un juicio.</li>
                </ul>
              </div>
              <!--panel 1-->
              <!-- panel 2-->
              <div class="tab-pane fade in" id="panel-re-2" role="tabpanel">
                <ul class="list- text-justify wow fadeInRight" data-wow-delay="0.6s">
                  <li class="list-item">Escucha con sinceridad las opiniones y participa al final de cada intervención sin interrumpir.</li>
                  <li class="list-item">Modera tu volumen y tono de voz cuando expongas tus argumentos con otra persona.</li>
                  <li class="list-item">Explica tu desacuerdo en caso de ser necesario, facilita al resto de las personas comprender tu punto de vista.</li>
                  <li class="list-item">Reconoce y valora cuando el trabajo está bien realizado.</li>
                  <li class="list-item">Identifica los errores como pasos de aprendizaje para alcanzar el éxito.</li>
                  <li class="list-item">Dale la oportunidad a tus compañeros de crecer, desarrollarse y contribuir a la  compañía, como en su entorno personal.</li>
                </ul>
              </div>
              <!--panel 2-->
            </div>
          </div>
        </div>
  </section>
</div>
<div class="streak streak-photo streak-md hm-indigo-light" style="background-image: url('/packages/assets/media/images/slide4.png?9012'); height:300px;">
    <div class="flex-center mask pattern-1">
        <div class="text-center white-text">
            <h2 class="h2-responsive mb-5"><strong>   </strong></h2>
        </div>
    </div>
</div>
<div class="container">
  <section id="attractions" class="section feature-box mt-4 mb-4">
        <!--Section heading-->
        <!-- <h1 class="section-heading my-5 pt-4 wow fadeIn" data-wow-delay="0.2s">Tips Éticos <i class="fa fa-balance-scale"></i></h1> -->
        <div class="row">
          <div class="col-md-5">
            <div class="">
                <div class="text-center white-text wow fadeInLeft" data-wow-delay="0.6s">
                    <img src="/packages/assets/media/images/valores/etica-text.png?234" class="img-fluid" alt="">
                </div>
            </div>
          </div>
          <div class="col-md-7">
            <ul class="nav md-pills nav-justified my-pills pills-secondary pills-etica" style="padding-left:3rem;padding-right:3rem;">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#panel-et-1" role="tab">Parte uno</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel-et-2" role="tab">Parte dos</a>
                </li>
            </ul>
            <div class="tab-content">
              <!-- panel 1-->
              <div class="tab-pane fade in show active" id="panel-et-1" role="tabpanel">
                <ul class="list- text-justify wow fadeInRight" data-wow-delay="0.6s">
                  <li class="list-item">Conoce como son las conductas de ética en DuPont.</li>
                  <li class="list-item">Si te encuentras en un dilema respecto a una decisión laboral, toma el tiempo de comentar tu preocupación con tu supervisor o algún colaborador. </li>
                  <li class="list-item">Solicita ayuda cuando no estés seguro de qué acción tomar.</li>
                  <li class="list-item">En caso de tratar con proveedores internos o externos, asegúrate de que todos los intercambios de dinero o especie estén justificados.</li>
                  <li class="list-item">Utiliza todos los bienes que te provee la compañía apropiadamente de acuerdo a los lineamientos. Pregunta o investiga sobre su uso adecuado. </li>
                  <li class="list-item">En caso de ya no hacer uso de alguno de los bienes de la compañía, devuélvelo íntegramente a los responsables en cuestión.</li>
                  <li class="list-item">Provee información verídica en todo momento.</li>
                </ul>
              </div>
              <!--panel 1-->
              <!-- panel 2-->
              <div class="tab-pane fade in" id="panel-et-2" role="tabpanel">
                <ul class="list- text-justify wow fadeInRight" data-wow-delay="0.6s">
                  <li class="list-item">No compartas información confidencial de la compañía.</li>
                  <li class="list-item">No tergiversar información para favorecer resultados de tu trabajo o de la compañía.</li>
                  <li class="list-item">Justifica todas las relaciones laborales que establezcas con futuros colaboradores o proveedores para no incurrir en conflicto de intereses.</li>
                  <li class="list-item">Asegúrate de actuar siempre de acuerdo a las leyes y regulaciones de tu localidad. Infórmate sobre las legislaciones que conciernen tu trabajo.</li>
                  <li class="list-item">No encubras ni toleres actos que deliberadamente violan normas éticas. </li>
                  <li class="list-item">Reporta cualquier caso de violación ética, tanto comportamiento como relación, que notes en tu entorno laboral.</li>
                  <li class="list-item">Recuerda que, aunque no actúes bajo beneficio propio, las acciones impropias también incurren en una conducta no ética.</li>
                </ul>
              </div>
              <!--panel 2-->
            </div>
          </div>
        </div>
  </section>
</div>
<div class="streak streak-photo streak-md hm-indigo-light" style="background-image: url('/packages/assets/media/images/slide5.png?9'); height:340px;">
    <div class="flex-center mask pattern-1">
        <div class="text-center white-text">
            <h2 class="h2-responsive mb-5"><strong>   </strong></h2><br>
        </div>

    </div>
</div>
@stop

@section('js')
<script src="/packages/assets/js/guia.js" charset="utf-8"></script>
<script src="/packages/assets/js/components.js" charset="utf-8"></script>
<script src="/packages/assets/js/main.js" charset="utf-8"></script>
@stop
