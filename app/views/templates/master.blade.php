<!DOCTYPE html5>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="keywords" content="prefiltros,motores,torreon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="/packages/assets/media/images/valores/nuestrosvalores.jpg">    <title>@yield('title')</title>
    <!-- Zona de archivos de estilo -->
    {{HTML::style('packages/libs/mdb/css/bootstrap.min.css')}}
    {{HTML::style('packages/libs/font-awesome-4.7.0/css/font-awesome.min.css')}}
  	{{HTML::style('packages/libs/mdb/css/mdb.min.css')}}
    {{HTML::style('packages/libs/mdb/css/style.css')}}
    {{HTML::style('packages/assets/css/main.css')}}
    {{HTML::style('/packages/assets/css/style.css?'.rand())}}
    {{HTML::style('packages/assets/css/preload.css')}}
    <link rel="stylesheet" href="/packages/libs/sweetalert2/sweetalert2.min.css">
  	@yield('css')
  </style>
</head>
<body class="intro-page pink-skin">
    <header>
      <!-- navbar-fixed-top scrolling-navbar -->
        <!--Navbar-->
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-pink scrolling-navbar" id="my-nav">
          <div class="container">
            <a class="navbar-brand" href="#">
              <img src="/packages/assets/media/images/valores/nuestrosvalores-white.png?v={{rand()}}" alt="" class="img-fluid icon-white" id="icon-nav">
              <img src="/packages/assets/media/images/valores/nuestrosvalores.png" alt="" class="img-fluid icon-black" id="icon-nav">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">

              </ul>
              <ul class="navbar-nav nav-flex-icons">

                @if(Auth::user())
                <li class="nav-item">
                  <a href="#our-valors" style="margin-top:1rem;" data-target="#mdl-login-register"   class="nav-link waves-effect waves-light"> <span class="">#NuestrosValores</span></a>
                </li>
                <li class="nav-item">
                  <a href="#cont-seg"  style="margin-top:1rem;" data-target="#mdl-login-register"   class="nav-link waves-effect waves-light"><i class="fa fa-list-ul"></i> <span class="">Guía de valores</span></a>
                </li>
                <li class="nav-item" style="margin-top:1rem;">
                  <a href="/logout"  class="nav-link waves-effect waves-light"><i class="fa fa-sign-in"></i> <span class="">Cerrar sesión</span></a>
                </li>
                <li class="nav-item">
		                <a href="/game" class="btn btn-outline-warning btn-rounded pull-right btn-action waves-effect waves-light btn-going-play" style="color: rgb(255, 255, 255); margin-left: 10px; margin-top: .4rem; transition: 0.6s;" href="#membresia"><i class="fa fa-gamepad"></i> Ir a juegos</a>
	              </li>
                <!--<li class="nav-item ancla">
                    <a href="game/memorama" class="nav-link waves-effect waves-light"> <i class="fa fa-gamepad"></i> <span class="">Ir a juegos</span></a>
                </li>-->
                @else
                <li class="nav-item">
                  <a href="#our-valors"   class="nav-link my-nav-link waves-effect waves-light"> <span class="">#NuestrosValores</span></a>
                </li>
                <li class="nav-item">
                  <a href="#cont-seg"   class="nav-link my-nav-link waves-effect waves-light"><i class="fa fa-list-ul"></i> <span class="">Guía de valores</span></a>
                </li>
                <li class="nav-item" style="margin-right:2rem;">
                  <a data-toggle="modal" data-target="#mdl-login-register"   class="nav-link waves-effect waves-light"><i class="fa fa-sign-in"></i> <span class="">Iniciar sesión</span></a>
                </li>
                @endif
              </ul>
            </div>
          </div>
      </nav>
        @yield("carousel-section")
</header>

<main>
     @yield('content-main')
</main>
<div class="modal fade" id="mdl-login-register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Modal cascading tabs-->
            <div class="modal-c-tabs">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs tabs-2 light-blue darken-3" role="tablist" style="background-color:#E31837!important;">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab"><i class="fa fa-user mr-1"></i> Iniciar sesión</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i class="fa fa-user-plus mr-1"></i> Registrarme</a>
                    </li>
                </ul>

                <!-- Tab panels -->
                <div class="tab-content">
                    <!--Panel 7-->
                    <div class="tab-pane fade in show active" id="panel7" role="tabpanel">

                        <!--Body-->
                        <form id="frm-login">
                          <div class="modal-body mb-1">
                              <div class="md-form form-sm">
                                  <i class="fa fa-envelope prefix"></i>
                                  <input type="text" id="email" name="email" class="form-control">
                                  <label for="email">Correo</label>
                              </div>

                              <div class="md-form form-sm">
                                  <i class="fa fa-lock prefix"></i>
                                  <input type="password" id="password" name="password" class="form-control">
                                  <label for="password">Contraseña</label>
                              </div>
                              <div class="text-center mt-2">
                                  <button class="btn btn-success" id="btn-login">Iniciar <i class="fa fa-sign-in ml-1"></i></button>
                              </div>
                          </div>
                        </form>
                        <!--Footer-->
                        <div class="modal-footer display-footer">
                            <button type="submit" class="btn btn-outline-danger waves-effect ml-auto" data-dismiss="modal">Cerrar <i class="fa fa-times-circle ml-1"></i></button>
                        </div>

                    </div>
                    <!--/.Panel 7-->

                    <!--Panel 8-->
                    <div class="tab-pane fade" id="panel8" role="tabpanel">

                        <!--Body-->
                        <div class="modal-body">
                            <form id="frm-register">
                              <div class="md-form form-sm">
                                  <i class="fa fa-envelope prefix"></i>
                                  <input type="email" id="email-r" name="emai-r" class="form-control" length="100">
                                  <label for="email-r">Correo</label>
                              </div>
                              <div class="md-form form-sm">
                                  <i class="fa fa-keyboard-o prefix"></i>
                                  <input type="text" id="nombre" name="nombre" class="form-control" length="100">
                                  <label for="nombre">Nombre(s)</label>
                              </div>

                              <div class="md-form form-sm">
                                  <i class="fa fa-keyboard-o prefix"></i>
                                  <input type="text" id="apellidos" name="apellidos" class="form-control" length="100">
                                  <label for="apellidos">Apellido(s)</label>
                              </div>

                              <div class="text-center form-sm mt-2">
                                  <button type="submit" class="btn btn-success" id="btn-register">Iniciar <i class="fa fa-sign-in ml-1"></i></button>
                              </div>

                        </div>
                        <!--Footer-->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger waves-effect ml-auto" data-dismiss="modal" id="btn-close" >Cerrar <i class="fa fa-times-circle ml-1"></i></button>
                        </div>
                        </form>
                    </div>
                    <!--/.Panel 8-->
                </div>

            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
</div>
     <!--Footer-->
     <footer class="page-footer elegant-color center-on-small-only fadeInUp" style="margin-top:-.4rem; padding-top:1.4rem">

         <!--Footer Links-->
        <div class="container-fluid">
             <div class="row">

                 <!--First column-->
                 <div class="col-md-12">
                     <p class="text-center">-<!--#NuestrosValores: Capaña para la capacitación de personal.--></p>
                 </div>
                 <!--/.First column-->
               </div>
         <!--/.Footer Links-->
        <hr>
            <!--Call to action-->
        <div class="row" style="padding-bottom:1rem;">
       			<div class="col-md-3">
       				<center>
       					<img src="/packages/assets/media/images/valores/nuestrosvalores-white.png" alt="" class="img-fluid wow fadeInLeft" id="logo-footer" style="width:10rem;height:5rem">
       				</center>
       			</div>
       			<div class="col-md-6 text-center">
              @if(Auth::user())
    						<a  href="/game" class="btn btn-rounded btn-action waves-effect waves-light wow fadeIn" style="color: rgb(255, 255, 255);transition: 0.6s; background-color:#E31837;" href="#membresia"><i class="fa fa-gamepad"></i> Ir a juegos</a>
              @else
                <a data-toggle="modal" data-target="#mdl-login-register" class="btn btn-rounded btn-action waves-effect waves-light wow fadeIn" style="color: rgb(255, 255, 255);transition: 0.6s; background-color:#E31837;" href="#membresia"><i class="fa fa-gamepad"></i> Ir a juegos</a>
              @endif
       				<!-- <br>
       				<center><h6 class="h6-responsive text-white">SuperNova | Todos los derechos reservados © 2017</h6></center> -->
       			</div>
       			<div class="col-md-3">
       				<center><a href="#carousel-example-1" class="btn-floating btn-large orange wow fadeInRight waves-effect waves-light my-nav-link" id="btn-return"><i class="fa fa-arrow-up"></i></a></center>
       			</div>
       	</div>
            <!--/.Call to action-->
    </div>
         <!--Copyright-->
         <div class="footer-copyright">
             <div class="container-fluid">
                 <span>Copyright: © {{date('Y')}} Supernovaapps S.A de C.V. All Rights Reserved <br>.</span>
             </div>
         </div>
         <!--/.Copyright-->

     </footer>
     <!--/.Footer-->

 <!-- Zona de archivos js -->
    {{HTML::script('packages/libs/mdb/js/jquery-3.1.1.min.js')}}
    {{HTML::script('packages/libs/mdb/js/popper.min.js')}}
    {{HTML::script('packages/libs/mdb/js/bootstrap.min.js')}}
    {{HTML::script('packages/libs/mdb/js/mdb.min.js')}}
    {{HTML::script('packages/libs/validation/jquery.validate.min.js')}}
    {{HTML::script('packages/libs/validation/additional-methods.min.js')}}
    {{HTML::script('packages/libs/validation/localization/messages_es.js')}}
    <script src="/packages/assets/js/dupont.js" charset="utf-8"></script>
    <script src="/packages/assets/js/user.js?{{rand()}}"></script>

        <script>
            //Animation init
            new WOW().init();

            //Modal
            $('#myModal').on('shown.bs.modal', function () {
              $('#myInput').focus()
            })

            // Material Select Initialization
            $(document).ready(function() {
                $('.mdb-select').material_select();
            });

            // MDB Lightbox Init
            $(function () {
                $("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");
            });
        </script>
    @yield('js')
<!--/.Navbar-->
</body>
</html>
