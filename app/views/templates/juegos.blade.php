<!DOCTYPE html5>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="keywords" content="prefiltros,motores,torreon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="/packages/assets/media/images/valores/nuestrosvalores.jpg">
    <title>@yield('title')</title>
    <!-- Zona de archivos de estilo -->
    {{HTML::style('packages/libs/mdb/css/bootstrap.min.css')}}
    {{HTML::style('packages/libs/font-awesome-4.7.0/css/font-awesome.min.css')}}
  	{{HTML::style('packages/libs/mdb/css/mdb.min.css')}}
    {{HTML::style('packages/libs/mdb/css/style.css')}}
    {{HTML::style('packages/assets/css/main.css')}}
    <link rel="stylesheet" href="/packages/libs/sweetalert2-master/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="/packages/assets/css/style-juegos.css?{{rand()}}">
  	@yield('css')
</head>
<body class="intro-page pink-skin">
    <header>
      <!-- navbar-fixed-top scrolling-navbar -->
      <!--Navbar-->
      <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-pink scrolling-navbar" id="my-nav">
        <div class="container">
          <a class="navbar-brand" href="/">
            <img src="/packages/assets/media/images/valores/nuestrosvalores-white.png?{{rand()}}" alt="" class="img-fluid" id="icon-nav">
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">

            </ul>
            <ul class="navbar-nav nav-flex-icons">
              <li class="nav-item">
                <a href="/logout"  class="nav-link waves-effect waves-light"><i class="fa fa-sign-in"></i> <span class="">Cerrar sesión</span></a>
              </li>
              <li class="nav-item ancla">
                  <a href="/" class="nav-link waves-effect waves-light"> <i class="fa fa-home"></i> <span class="">Ir a inicio</span></a>
              </li>
            </ul>
          </div>
        </div>
    </nav>
        <!--/.Navbar-->
</header>
<main>
<!-- section for games -->
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">
          <div class="card card-cascade narrower" id="my-card">
                <!--Card image-->
                <div class="view overlay hm-white-slight">
                    <img src="/packages/assets/media/images/valores/nuestrosvalores.jpg" class="img-fluid" alt="Nuestros valores">
                    <a>
                        <div class="mask waves-effect waves-light"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-body text-center">
                    <!--Title-->
                    <h4 class="card-title"><strong>Bienvenid@ {{Auth::user()->nombre.' '.Auth::user()->apellidos}}</strong></h4>
                    <!--Text-->
                    <hr>
                    <p><a href="/game" class="btn btn-danger"><i class="fa fa-gamepad"></i> Ver juegos</a></p>
                    <p><a href="/estadisticas" class="btn bg-blue" style="background-color: #4285F4!important;"><i class="fa fa-list"></i> Ver puntuaciones</a></p>

                </div>
                <!--/.Card content-->

            </div>
        </div>
        <div class="col-md-9" id="marco">
          <div class="app">
            @yield('content-main')
          </div>
        </div>
      </div>
  </div>
<!-- end section for games -->
</main>


     <!--Footer-->
     <footer id="my-footer" class="page-footer elegant-color center-on-small-only fadeInUp">

         <!--Footer Links-->
        <!-- <div class="container-fluid">
             <div class="row">-->

                 <!--First column-->
                 <!--<div class="col-md-6">
                     <h5 class="title">Nuestros Valores</h5>
                     <p>#NuestrosValores: Capaña para la capacitación de personal.</p>
                 </div>-->
                 <!--/.First column-->

                 <!--Second column-->
              <!--   <div class="col-md-6">

                </div>-->
                 <!--/.Second column-->
          <!--   </div>
         </div>-->
         <!--/.Footer Links-->
      <!--   <hr>-->

            <!--Call to action-->
            <!--<div class="call-to-action">
                <ul>
                    <li>
                        <h5>Register for free</h5></li>
                    <li><a href="" class="btn btn-danger">Sign up!</a></li>
                </ul>
            </div>-->
            <!--/.Call to action-->

         <!--Copyright-->
         <div class="footer-copyright">
             <div class="container-fluid">
                 ©<span>Copyright: © {{date('Y')}} Supernovaapps S.A de C.V. All Rights Reserved <br>.</span>
             </div>
         </div>
         <!--/.Copyright-->

     </footer>
     <!--/.Footer-->

 <!-- Zona de archivos js -->
    {{HTML::script('packages/libs/mdb/js/jquery-3.1.1.min.js')}}
    {{HTML::script('packages/libs/mdb/js/popper.min.js')}}
    {{HTML::script('packages/libs/mdb/js/bootstrap.min.js')}}
    {{HTML::script('packages/libs/mdb/js/mdb.min.js')}}
    <script src="/packages/libs/sweetalert2-master/dist/sweetalert2.min.js" charset="utf-8"></script>
    <script src="/packages/libs/phaser.min.js"></script>
    <script src="/packages/assets/js/juegos/standard.js?{{rand()}}"></script>
    <script src="/packages/assets/js/user.js"></script>
    @yield('js')
<!--/.Navbar-->
</body>
</html>
