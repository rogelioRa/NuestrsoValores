@extends('templates.juegos')

@section("title") Juegos capacitación @stop

@section('css')
<link rel="stylesheet" href="/packages/libs/bootstrap-table-master/dist/bootstrap-table.min.css">
@stop

@section('content-main')

<iframe src="/get-reporte-view" width="100%" height="100%" bordered="0" style="border:none;"></iframe>
@stop
@section('js')
<script src="/packages/libs/bootstrap-table-master/dist/bootstrap-table.min.js" charset="utf-8"></script>
<script src="/packages/libs/bootstrap-table-master/dist/bootstrap-table-locale-all.min.js" charset="utf-8"></script>
<script src="/packages/libs/bootstrap-table-master/dist/locale/bootstrap-table-ca-ES.min.js" charset="utf-8"></script>
<script src="/packages/libs/bootstrap-table-master/dist/extensions/export/bootstrap-table-export.min.js" charset="utf-8"></script>
<script src="/packages/libs/charts/code/highcharts.js" charset="utf-8"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
  $(function(){
    var datos = {{$reporte}};
    $('#table-report').bootstrapTable({ data : datos});
  });
</script>
<!-- <script type="text/javascript">
  var estadisticas = {{$estadisticas}};
  function getKeys(){
    var keys = [];
    $.each(estadisticas,function(clave,valor){
        keys.push(valor.nombre);
    });
    return keys;
  }
  function getMinutsFromSeconds(time){
    console.log(time);
    if(time != 0){
      var minutes = Math.floor( time / 60 );
      var seconds = time % 60; 
      //Anteponiendo un 0 a los minutos si son menos de 10
      minutes = minutes < 10 ? '0' + minutes : minutes 
      //Anteponiendo un 0 a los segundos si son menos de 10
      seconds = seconds < 10 ? '0' + seconds : seconds;
      var result = minutes + ":" + seconds;  // 161:30
      return result;
    }
  }
  function getKeysGame(){
    keys = [];
    $.each(estadisticas[0],function(clave,valor){
      if(clave!="user_id" && clave !="nombre"){
        keys.push(clave);
      }
    });
    return keys;
  }
  function getSeries(){
    // console.log([ {name:"memorama",data:[1,2]} ]);
    // return [ {name:"memorama",data:[1,2]} ];
    console.log(estadisticas);
    var series = [];
    var keys = getKeysGame();
    for(var i=0;i<keys.length;i++){
      var serie    = {};
      serie.name   = keys[i];
      serie.data   = [];
      $.each(estadisticas,function(index,object){
        $.each(object,function(clave,valor){
          if(keys[i]==clave){
            console.log(valor);
            valor = (valor==null)? 0 : valor;
            console.log(valor);
            serie.data.push((parseFloat(valor)));
          }
        });
      });
      console.log(serie);
      series.push(serie);
    }
    console.log(series);
    return series;
  }
  Highcharts.chart('grafica', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Mejores tiempos obtenidos por usuario'
    },
    subtitle: {
        text: '#NuestrosValores'
    },
    xAxis: {
        categories: getKeys(),
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Tiempo (Min)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} seg</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series:getSeries()
});
</script> -->
@stop
