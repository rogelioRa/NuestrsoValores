@extends('templates.juegos')
@section("title") Juegos capacitación @stop
@section('content-main')
<div id="app-game">

</div>
@stop
@section('js')
<script src="/packages/assets/js/juegos/game.js" charset="utf-8"></script>
<script src="/packages/assets/js/juegos/preguntas.js" charset="utf-8"></script>
<script src="/packages/assets/js/juegos/conceptos.js" charset="utf-8"></script>
<script src="/packages/assets/js/juegos/{{$game}}.js?{{rand()}}" charset="utf-8"></script>
@stop
