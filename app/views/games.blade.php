 @extends('templates.juegos')
 @section("title") Juegos capacitación @stop
 @section('content-main')
 <div class="container" style="margin-bottom:3rem;">
   <div class="row" style="">
   @if(Auth::user()->rol!="admin")
     @foreach($juegos as $juego)
       @if($juego->nombre!="memorama")
       <div class="col-md-4">
          <!-- Card -->
          <div class="card card-image my-card-image unactive animated zoomIn" style="background-image: url(/packages/assets/media/images/wallgame.png);">
            <!-- Content -->
            <div class="text-white text-center align-items-center rgba-black-strong">
              <h6 class="orange-text"><i class="fa fa-"></i> Próximamente</h6>
              <hr>
              <h5 class="card-title pt-2"><i class="fa fa-gamepad"></i> {{$juego->alias}}</h5>
              <p></p>
              <a class="btn btn-primary btn-rounded btn-going-game disabled"  href="game/{{$juego->nombre}}">Jugar</a>
            </div>
            <!-- Content -->
          </div>
          <!-- Card -->
       </div>
       @else
       <div class="col-md-4">
          <!-- Card -->
          <div class="card card-image my-card-image animated zoomIn" style="background-image: url(/packages/assets/media/images/wallgame.png);">
            <!-- Content -->
            <div class="text-white text-center align-items-center rgba-black-light">
              <!-- <h5 class="pink-text"><i class="fa fa-gamepad"></i> sopa de letras</h5> -->
              <h3 class="card-title pt-2"><i class="fa fa-gamepad"></i> {{$juego->alias}}</h3>
              <p></p>
              @if($juego->cantidad>=10)
              <a class="btn btn-primary btn-rounded btn-going-game btn-limit">Jugar</a>
              @else
              <a class="btn btn-primary btn-rounded btn-going-game" href="game/{{$juego->nombre}}">Jugar</a>
              @endif
            </div>
            <!-- Content -->
          </div>
          <!-- Card -->
       </div>
       @endif
     @endforeach
     @else
      @foreach($juegos as $juego)
      <div class="col-md-4">
         <!-- Card -->
         <div class="card card-image my-card-image animated zoomIn" style="background-image: url(/packages/assets/media/images/wallgame.png);">
           <!-- Content -->
           <div class="text-white text-center align-items-center rgba-black-light">
             <!-- <h5 class="pink-text"><i class="fa fa-gamepad"></i> sopa de letras</h5> -->
             <h3 class="card-title pt-2"><i class="fa fa-gamepad"></i> {{$juego->alias}}</h3>
             <p></p>
             @if($juego->cantidad>=10)
             <a class="btn btn-primary btn-rounded btn-going-game btn-limit">Jugar</a>
             @else
             <a class="btn btn-primary btn-rounded btn-going-game" href="game/{{$juego->nombre}}">Jugar</a>
             @endif
           </div>
           <!-- Content -->
         </div>
         <!-- Card -->
      </div>
      @endforeach
     @endif
   </div>
 </div>
 @stop
 @section('js')
 <script type="text/javascript">
   $(".btn-limit").click(function(){
     swal(
        'Juego bloqueado!',
        'Haz llegado al límite de veces jugadas en este juego!',
        'info'
      )
   });
 </script>
 @stop
