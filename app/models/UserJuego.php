<?php


class UserJuego extends Eloquent{
	//public $timestamps = false;

	protected $table = 'usersJuegos';
  protected $fillable =['tiempo','aciertos','id_user','id_juego'];

  public function User(){
      return $this->belongsTo('User');
  }
  public function Juego(){
      return $this->belongsTo('Juego');
  }

}
