<?php


class Juego extends Eloquent{


	protected $table = 'juegos';
  protected $fillable =['nombre'];

  public function UserJuego(){
      return $this->hasMany('UserJuego','id_juego');
  }
}
